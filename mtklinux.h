#ifndef MTKLINUX_H_
#define MTKLINUX_H_


struct interface_ops mtklinux_ops;


//////////////// DEPRECATED ////////////////
//#define DBG

int mtkl_read(unsigned char *buf, int len);
int mtkl_write(unsigned char *buf, int len);
int mtkl_flush();
int mtkl_drain(void);

int mtkl_echo(unsigned char *ibuf, unsigned char *obuf, int len);
int mtkl_test(unsigned char *ibuf, unsigned char *obuf, int len);
int mtkl_chk_D1(unsigned char *ibuf, unsigned char *check, int len);

#endif
