

struct interface_ops {
	int	(*open)(void);
	int	(*close)(void);
	int	(*read)(unsigned char *buf, int len);
	int	(*write)(unsigned char *buf, int len);
	int	(*flush)(void);
	int	(*clear)(void);
};

void mtk_set_interface(struct interface_ops *ops);

int mtk_open(void);
int mtk_close(void);
int mtk_read(unsigned char *buf, int len);
int mtk_write(unsigned char *buf, int len);
int mtk_flush(void);
int mtk_clear(void);


int mtk_write_char(unsigned char c);
int mtk_read_char(void);
int mtk_read_word(void);
