#include <stdio.h>

#include "mtkda.h"
#include "target.h"
#include "common.h"

/**
 * Read 32 bits from the memory space of the SOC
 *
 * addr: 	Address to read from
 * len: 	Number of 32 bit words to read
 * check:	Array of 4 byte word of expected values read
 *
 * returns:	-1 if address alignment error
 * 		-2 if len == 1
 * 		-3 if overflow attac check fails
 * 		-4 if check not correct
 * 		 0 success
 **/
short int target_read32(unsigned int addr, unsigned int len, unsigned char *check)
{
	unsigned char buf[255];
	int i, j = 0;
	short int status;

	unsigned char base_addr[4], length[4];

	uint_to_char(addr, base_addr);
	uint_to_char(len, length);

	mtkl_test("\xD1", "\xD1", 1);
	mtkl_test(base_addr, base_addr, 4);
	mtkl_test(length, length, 4);
	mtkl_read(buf, 2);

	status = char16_to_short(buf);
	if (status != 0) {
		printf("read32 error - status %d\n", status);
		return status;
	}
	for (i = 0; i < len; i++) {
		mtkl_read(buf, 4);
#ifdef DBG
		printf("%s - ", __func__);
		for (j = 0; j < 4; j++)
			printf("%.2x %.2x\t", buf[j], check[j]);
		printf("\n");
		printf("%x %x\n", char32_to_int(buf), char32_to_int(check));
#endif
		if (bcmp(buf, check, 4) != 0) {
			printf("read32 error - data(%d) %x - check %x\n",
					i, char32_to_int(buf), char32_to_int(check));
			return -4;
		}
		check += 4;
	}
	mtkl_read(buf, 2);

	status = char16_to_short(buf);
	return status;
}

short target_get_config(unsigned char *config)
{
	unsigned char buf[2];
	unsigned int conf;
	short status;

	mtkl_test("\xD8", "\xD8", 1); // CMD_GET_TARGET_CONFIG
	mtkl_read(config, 4); // 	0x1 -> SBC_EN
			//	0x2 -> SLA_EN
			//	0x4 -> DAA_EN
	conf = char32_to_int(config);

	mtkl_read(buf, 2); // status
	status = char16_to_short(buf);

	if (status == 0) {
		switch(conf) {
			case 0x1:
				printf("Config SBC enabled\n");
			case 0x2:
				printf("Config SLA enabled\n");
			case 0x4:
				printf("Config DAA enabled\n");
			default:
				break;
		}
	}

	return status;
}

short int target_write32(unsigned int addr, unsigned int len, unsigned char *data)
{
	unsigned char buf[2], base_addr[4], length[4];
	short status;
	int i;

	uint_to_char(addr, base_addr);
	uint_to_char(len, length);

	// WRITE 32
	mtkl_test("\xD4", "\xD4", 1);
	mtkl_test(base_addr, base_addr, 4);
	mtkl_test(length, length, 4); // length
	mtkl_read(buf, 2); // status	-1 -> bad addr alignment
				//	-2 -> length == 0
				//	-3 -> overflow attack found
	status = char16_to_short(buf);

	switch (status) {
		case -1:
			printf("error write32 bad address alignment\n");
			return status;
		case -2:
			printf("error write32 len == 0\n");
			return status;
		case -3:
			printf("error write32 overflow attack found\n");
			return status;
		default:
			break;
	}

	for (i = 0; i < len; i++) {
		mtkl_test(data, data, 4);
		data += 4;
	}

	mtkl_read(buf, 2); // status

	return char16_to_short(buf);
}

unsigned char target_check_bootloader_ver(void)
{
	unsigned char buf;

	mtkl_write("\xFE", 1);
	mtkl_read(&buf, 1);

	return buf;
}

// TODO put addr, da_len and at least sig_len in a struct and pass this struct
// TODO should we compute the checksum ourselve? do we have the time to do so?
short target_send_download_agent(unsigned int addr, unsigned int da_len, unsigned int sig_len, unsigned char *checksum)
{
	unsigned char buf[2], base_addr[4], da_length[4], sig_length[4], obuf[1024];
	short status;
	int i;

	uint_to_char(addr, base_addr);
	uint_to_char(da_len, da_length);
	uint_to_char(sig_len, sig_length);

	// SEND_DA
	mtkl_test("\xD7", "\xD7", 1);
	mtkl_test(base_addr, base_addr, 4); // da_addr (seems to be reset to 0x80001000)
	mtkl_test(da_length, da_length, 4); // da_length
	mtkl_test(sig_length, sig_length, 4); // Signature length
	mtkl_read(buf, 2); // status
	status = char16_to_short(buf);

	if (status != 0) {
		printf("error setting download agent address\n");
		return status;
	}

	// TODO this should be passed from the application!
	mtkda_open("./firmware/V3.1304.0.119-DA.bin", V3_1304_0_119);

	for (i = 0; i < (da_len/1024); i++) {
		mtkda_read(obuf, 1024);
		mtkl_write(obuf, 1024);
	}

	mtkda_read(obuf, (da_len % 1024));
	mtkl_write(obuf, (da_len % 1024));
	//mtkl_flush();

	mtkl_read(buf, 2); // checksum
	if (bcmp(buf, checksum, 2) != 0) {
		printf("error checksum %d wrong!\n", char16_to_short(buf));
		return -1;
	}
	mtkl_read(buf, 2); // status of usbdl_verify_da
			//	0x2003 -> initkey fail
			//	0x2006 -> da relocate size not enough
			//	0x2001 -> da image sig verify fail
			//	0x2002 -> da image no mem fail
			//	0x0000 -> verify ok or da validation disabled
	status = char16_to_short(buf);

	switch(status) {
		case 0x2001:
			printf("send da: image signature verify fail\n");
			break;
		case 0x2002:
			printf("send da: image no memeory fail\n");
			break;
		case 0x2003:
			printf("send da: initkey fail\n");
			break;
		case 0x2006:
			printf("send da: relocate size not enough\n");
			break;
		case 0x0000:
			break;
		default:
			printf("send da: unknown status return\n");
	}
	return status;
}

void target_jump_to_da(void)
{
	mtkl_test("\xD5", "\xD5", 1); // jump to da
}

#ifndef MT6577
/**
 * Dummy function in preloader
 **/
short target_uart1_log_enable()
{
	unsigned char buf[2];

	mtkl_test("\xDB", "\xDB", 1);
	mtkl_read(buf, 2); // status
	return char16_to_short(buf);
}

short target_power_init(void)
{
	unsigned char buf[2];

	mtkl_test("\xC4", "\xC4", 1);
	mtkl_test("\x80\x00\x00\x00", "\x80\x00\x00\x00", 4); // config
	mtkl_test("\x00\x00\x00\x00", "\x00\x00\x00\x00", 4); // timeout
	mtkl_read(buf, 2); // status

	return char16_to_short(buf);
}

short target_power_read16(unsigned int addr, unsigned char *check)
{
	unsigned char buf[2];
	short status;
	unsigned char base_addr[2];

	short_to_char(addr, base_addr);

	mtkl_test("\xC6", "\xC6", 1);
	mtkl_test(base_addr, base_addr, 2); // address
	mtkl_test(NULL, "\x00\x00", 2); // parameter check

	mtkl_read(buf, 2); // pmic operation status
	status = char16_to_short(buf);

	if (status) {
		printf("error reading in pmic operation\n");
		return status;
	}

	mtkl_read(buf, 2); // data from address

	if (bcmp(buf, check, 2) != 0) {
		printf("power read16 error - data %x - check %x\n",
				char16_to_short(buf), char16_to_short(check));
		return -2;
	}

	return 0;
}

short target_power_deinit(void)
{
	unsigned char buf[2];

	mtkl_test("\xC5", "\xC5", 1);
	mtkl_read(buf, 2);

	return char16_to_short(buf);
}
#endif

