
#include "interface.h"

static struct interface_ops *mtk_ops;

void mtk_set_interface(struct interface_ops *ops)
{
	mtk_ops = ops;
}

int mtk_open(void) {
	return mtk_ops->open();
}

int mtk_close(void) {
	return mtk_ops->close();
}

int mtk_read(unsigned char *buf, int len) {
	return mtk_ops->read(buf, len);
}

int mtk_write(unsigned char *buf, int len) {
	return mtk_ops->write(buf, len);
}

int mtk_flush(void) {
	return mtk_ops->flush();
}

int mtk_clear(void) {
	return mtk_ops->clear();
}

/*
 * Helper functions
 */

int mtk_write_char(unsigned char c)
{
	return mtk_write(&c, 1);
}

int mtk_read_char(void)
{
	unsigned char c;
	int ret;
	ret = mtk_read(&c, 1);
	if (ret <= 0)
		return -1;
	return c;
}

/*
 * usbdl_put_word in the preloader works in big-endian mode, aka 
 * high byte first.
 */
int mtk_read_word(void)
{
	unsigned char c[2];
	int ret;

	ret = mtk_read(&c[0], 2);
	if (ret <= 0)
		return -1;

	/* FIXME: support mutiple endianes of the host */
	return (c[0] << 8 | c[1]);
}
