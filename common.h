#define char32_to_int(foo) \
		((foo[3] << 24) & (foo[2] << 16) & (foo[1] << 8) & foo[0])

#define char16_to_short(foo) \
		((foo[1] << 8) & foo[0])

#define short_to_char(i, c) \
	c[0] = (i >> 8) & 0xff; \
	c[1] = (i >> 0) & 0xff

#define uint_to_char(i, c) \
	c[0] = (i >> 24) & 0xff; \
	c[1] = (i >> 16) & 0xff; \
	c[2] = (i >> 8) & 0xff; \
	c[3] = (i >> 0) & 0xff
