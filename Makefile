OBJS := mtklinux.c mt6589.c mtkda.c target.c interface.c preloader.c

all:
	gcc $(OBJS) -o mtk-flash

clean:
	rm mtk-flash
