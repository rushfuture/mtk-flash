#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interface.h"
#include "preloader.h"
#include "mtklinux.h"
#include "mtkda.h"
#include "target.h"
#include "i90_magic.h"

int main()
{
	int fd, fd_da, i, ret;
	unsigned char ibuf[2048];
	unsigned char obuf[4096];
	unsigned char emmc_id[16];

	//TODO we could read this out of a scatter-loading file
	unsigned char addr_boot_i90[8] = {0x00, 0x00, 0x00, 0x00, 0x02, 0x78, 0x00, 0x00};
	//TODO length_boot_i90 might depend on the size of boot.img
	unsigned char length_boot_i90[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x4C, 0x20, 0x00};

	/* at the moment we simply set the linux-interface */
	mtk_set_interface(&mtklinux_ops);

	printf("waiting for device to appear\n");

	if (mtk_open()) {
		fprintf(stderr, "error opening port\n");
		exit(1);
	}

	printf("device opened\n");

	ret = preloader_wait_for_ready();
	if (ret < 0) {
		fprintf(stderr, "device not ready\n");
		mtk_close();
		exit(1);
	}

	printf("device ready and communication open\n");

	ret = preloader_handshake();
	if (ret < 0) {
		fprintf(stderr, "preloader handshake failed\n");
		mtk_close();
		exit(1);
	}

	printf("entered usb download mode\n");

	memset(ibuf, 0, 255);

	ret = preloader_get_hw_code();
	if (ret < 0) {
		fprintf(stderr, "could not read hw code\n");
		mtk_close();
		exit(1);
	}
	printf("HW Code: 0x%x\n", ret);


	struct preloader_hw_sw_ver hw;

	ret = preloader_get_hw_sw_version(&hw);
	if (ret < 0) {
		fprintf(stderr, "could not read hw versions\n");
		mtk_close();
		exit(1);
	}

	printf("HW sub-Code: 0x%x\n", hw.hw_subcode);
	printf("HW Version: 0x%x\n", hw.hw_version);
	printf("SW Version: 0x%x\n", hw.sw_version);


	if (target_uart1_log_enable())
		printf("error enabling UART1 logging\n");

	if (target_power_init())
		printf("error enabling init power\n");


	if (target_power_read16(0x000E, "\x00\x01") != 0)
		printf("error reading power at address %#x\n", 0x000E);

	if (target_power_deinit() != 0)
		printf("error deinit power\n");

	if (target_write32(0x10000000, 1, "\x22\x00\x22\x24"))
		printf("error wirting to address %#x\n", 0x10000000);

	if (target_check_bootloader_ver() != 0x01)
		printf("error unexpected bootloader version\n");

	target_read32(0x10000000, 1, "\x00\x00\x00\x24"); // wachtdog mode register
	target_read32(0x10000004, 1, "\x00\x00\x50\x00"); // watchdog length register
	target_read32(0x10000008, 1, "\x00\x00\x00\x00"); // watchdog restart register
	target_read32(0x1000000c, 1, "\x00\x00\x00\x00"); // watchdog status register
	target_read32(0x10000010, 1, "\x00\x00\x0F\xFC"); // watchdog reset pulse width register
	target_read32(0x10000014, 1, "\x00\x00\x00\x00"); // software watchdog reset register
	target_read32(0x10000018, 1, "\x00\x00\x00\x00"); // system software reset register

	if (target_get_config(ibuf) != 0)
		printf("error reading target config\n");
	else
		printf("We use config %#x\n", ibuf[0]);

	mtkl_test("\xFF", "\xFF", 1); // command not implemented!

	if (target_check_bootloader_ver() != 0x01)
		printf("error unexpected bootloader version\n");


	// SEND_DA
	printf("OK, we put the Download Agent in SRAM...\n");
	if (target_send_download_agent(0x12000000, 0x229D0, 0x100, "\x46\x34") != 0)
		printf("error sending download agent!\n");
	else
		printf("Download Agent ready!\n");

	if (target_uart1_log_enable())
		printf("error enabling UART1 logging\n");


	printf("jump to DA!\n");
	target_jump_to_da();

	mtkl_test("\x12\x00\x00\x00", "\x12\x00\x00\x00", 4);
	mtkl_read(ibuf, 6);

	printf("read eMMC/Flash ID\n");

	mtkl_write("\xFF", 1);
	mtkl_write("\x01", 1);
	mtkl_write("\x00\x08", 2);
	mtkl_write("\x00", 1);
	mtkl_write("\x70\x07\xFF\xFF", 4);
	mtkl_write("\x00\x00\x00\x50", 4);
	mtkl_write("\x02", 1);
	mtkl_write("\x00", 1);

	mtkl_test(NULL, "\x00\x00\x00\x01", 4);

	for (i=0; i<4; i++) {
		mtkl_read(ibuf, 4);
		emmc_id[0+(i*4)] = ibuf[3];
		emmc_id[1+(i*4)] = ibuf[2];
		emmc_id[2+(i*4)] = ibuf[1];
		emmc_id[3+(i*4)] = ibuf[0];
	}

	printf("eMMC ID: ");
	for (i=0; i<16; i++) {
		printf("%.2x", emmc_id[i]);
	}
	printf("\n");

// in:	10	00000000000000000000
	mtkl_read(ibuf, 10);

// out:	4	00000034
	mtkl_write("\x00\x00\x00\x34", 4); // Flash ID Ack = 34

// out:	1	e8
	mtkl_write("\xE8", 1);

// out:	4	0000000a
	mtkl_write("\x00\x00\x00\x0A", 4);

// in:	1	5a
// in:	4	000000b8
	mtkl_test(NULL, "\x5A", 1);
	mtkl_read(ibuf, 4);//	mtkl_test(NULL, "\x00\x00\x00\xB8", 4);

// out:	1	5a
	mtkl_write("\x5A", 1);
	printf("write magic binary stream\n");

	// TODO check that this is always the same!
// out:	184	0100000002020000090000000100000090014a48414732650400000000000000
// 		0100000002020000090000000100000090014a484147326504000000000000000200000000000000aea3020000cc00cc00cc00cc08000000a444685500000011e18674f0013206c0a08c099f3f6340034223641188880000eeeeeeee00000000000000001006000100000007000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000100c30002000600030002000a00ff003f00000006000000
	mtkl_write(magic_binary_boot_i90, 184);

// in:	2	16f5
	mtkl_test(NULL, "\x16\xF5", 2); // might be check sum!

// out:	1	5a
	mtkl_write("\x5A", 1); // check sum ok (?)

// out:	4	80000001
	mtkl_write("\x80\x00\x00\x01", 4);

// in:	4	00000000
	mtkl_test(NULL, "\x00\x00\x00\x00", 4);

// in:	1	02
	mtkl_test(NULL, "\x02", 1);

// in:	1	00
	mtkl_test(NULL, "\x00", 1);

// in:	4	40000000
	mtkl_test(NULL, "\x40\x00\x00\x00", 4);

// out:	4	82038000
	mtkl_write("\x82\x03\x80\x00", 4); // GetNandTable(), base=0x82038000

// out:	4	000014b8
	mtkl_write("\x00\x00\x14\xB8", 4); // GetNandTable(), base=..., length=0x14B8

// out:	4	00001000
	mtkl_write("\x00\x00\x10\x00", 4);

// in:	1	5a
	mtkl_test(NULL, "\x5a", 1);

	// TODO this is different depending on what? - not on device nor on boot.img version nor on preloader
// out:	4096	37000000ec007300ffffffff1000000020000000000200000000000003000000 --> DA@0x16020
	//mtkda_seek(V3_1304_0_119, MAGIC_1);
	//mtkda_read(obuf, 4096);
	mtkl_write(i90_magic_1, 4096);

// in:	1	5a
	mtkl_test(NULL, "\x5a", 1);

// out:	1208	b8 27 02 12 00 00 00 00 00 00 00 00 26 00 00 00 20 00 c2 00 ff ff ff ff 40 00 00 00 40 00 00 00 -> DA@0x9f884
	mtkda_seek(V3_1304_0_119, MAGIC_2);
	mtkda_read(obuf, 1208);
	mtkl_write(obuf, 1208);

// in:	1	5a
	mtkl_test(NULL, "\x5a", 1);

// in:	1	5a
	mtkl_test(NULL, "\x5a", 1);

// out:	1	5a
	mtkl_write("\x5A", 1);

// in:	4	00000bc3
	mtkl_test(NULL, "\x00\x00\x0B\xC3", 4);

// in:	1	00
	mtkl_test(NULL, "\x00", 1);

// in:	1	08
	mtkl_test(NULL, "\x08", 1);

// in:	2	ffff
	mtkl_test(NULL, "\xFF\xFF", 2);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000bc4
	mtkl_read(ibuf, 4);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	2	ffff
	mtkl_read(ibuf, 2);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	2	0000
	mtkl_read(ibuf, 2);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00200000 --> EMMC_BOOT1_SIZE or 2
	mtkl_read(ibuf, 4);
	printf("EMMC_BOOT1_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00200000 --> EMMC_BOOT2_SIZE or 1
	mtkl_read(ibuf, 4);
	printf("EMMC_BOOT2_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00200000 --> EMMC_RPMB_SIZE
	mtkl_read(ibuf, 4);
	printf("EMMC_RPMB_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00000000 --> EMMC_GP1_SIZE or 4
	mtkl_read(ibuf, 4);
	printf("EMMC_GP1_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00000000 --> EMMC_GP2_SIZE or 3
	mtkl_read(ibuf, 4);
	printf("EMMC_GP2_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00000000 --> EMMC_GP3_SIZE or 2
	mtkl_read(ibuf, 4);
	printf("EMMC_GP3_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00000000 --> EMMC_GP4_SIZE or 1
	mtkl_read(ibuf, 4);
	printf("EMMC_GP4_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// in:	4	00000003 --> EMMC_USER_SIZE 1
	mtkl_read(ibuf, 4);

// in:	4	ab800000 --> EMMC_USER_SIZE 2 => 117 Gb
	mtkl_read(ibuf+4, 4);
	printf("EMMC_USER_SIZE = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3],
			ibuf[4], ibuf[5], ibuf[6], ibuf[7]);

// in:	4	484a0190 --> flash ID
	mtkl_read(ibuf, 4);

// in:	4	65324741 --> flash ID
	mtkl_read(ibuf+4, 4);

// in:	4	a2010204 --> flash ID
	mtkl_read(ibuf+8, 4);

// in:	4	8d70cbbc --> flash ID
	mtkl_read(ibuf+14, 4);

	printf("flash ID = 0x");
	for (i=0; i<16; i++) {
		printf("%.2x", ibuf[i]);
	}
	printf("\n");

// in:	1	02
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	4	00000c4a
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	4	00020000
	mtkl_read(ibuf, 4);

// in:	4	00000000
	mtkl_read(ibuf, 4);

// in:	1	02
	mtkl_read(ibuf, 1);

// in:	1	00
	mtkl_read(ibuf, 1);

// in:	4	40000000
	mtkl_read(ibuf, 4);

// in:	1	7f --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("Some magic numbers different to each plattform:\n");
	printf("%#.2x ", ibuf[0]);

// in:	1	f3 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	7b --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	4b --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	e7 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	bc --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	81 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	cd --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	61 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	92 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	97 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	4f --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	33 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	55 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	01 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x ", ibuf[0]);

// in:	1	a2 --> different on different platforms
	mtkl_read(ibuf, 1);
	printf("%#.2x\n", ibuf[0]);

// in:	1	5a
	mtkl_test(NULL, "\x5A", 1);

// in:	4	00000c06
	mtkl_read(ibuf, 4);

// in:	4	00000409
	mtkl_read(ibuf, 4);

// in:	1	c1
	mtkl_read(ibuf, 1);

// out:	1	a5 --> cmd=165
	mtkl_write("\xA5", 1);

// in:	1	5a
	mtkl_test(NULL, "\x5A", 1);

// in:	4	00000790 --> Reading PTB
	mtkl_read(ibuf, 4);
	printf("PTB = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// out:	1	5a
	mtkl_write("\x5A", 1);

// in:	1936	5052454c4f4144455200000000000000090000000f0000000000000000000000 --> different!
	mtkl_read(ibuf, 1936);

// out:	1	5a
	mtkl_write("\x5A", 1);

// out:	1	72 --> cmd=114
	mtkl_write("\x72", 1);

// in:	1	5a
	mtkl_test(NULL, "\x5A", 1);

// in:	1	01 --> USB Speed Status(1)
	mtkl_read(ibuf, 1);

// out:	1	e0 --> cmd=224
	mtkl_write("\xE0", 1);

// out:	4	00000000
	mtkl_write("\x00\x00\x00\x00", 4);

// in:	1	5a
	mtkl_test(NULL, "\x5A", 1);

// out:	1	b4 --> cmd=180
	mtkl_write("\xB4", 1);

// out:	4	00000000
	mtkl_write("\x00\x00\x00\x00", 4);

// in:	4	00001799
	mtkl_read(ibuf, 4);

// out:	1	a5 --> cmd=165
	mtkl_write("\xA5", 1);

// in:	1	5a
	//mtkl_test("\xA5", "\x5A", 1);
	mtkl_test(NULL, "\x5A", 1);

// in:	4	00000790 --> Reading PTB
	mtkl_read(ibuf, 4);
	printf("PTB = %#.2x%.2x%.2x%.2x\n", ibuf[0], ibuf[1], ibuf[2], ibuf[3]);

// out:	1	5a
	mtkl_write("\x5A", 1);

// in:	1936	5052454c4f4144455200000000000000090000000f0000000000000000000000 --> different!
	mtkl_read(ibuf, 1936);

// out:	1	5a
	mtkl_write("\x5A", 1);

// out:	1	61 --> cmd=97 -> SDMMCWriteImage
	printf("SDMMCWriteImage\n");
	mtkl_write("\x61", 1);

// out:	1	00
	mtkl_write("\x00", 1);

// out:	1	08 --> part=8
	printf("Partition= %x\n", 0x08);
	mtkl_write("\x08", 1);

// out:	8	0000000002780000 --> addr: 027800000 
	printf("Addr = %x%x%x%x%x%x%x%x\n", addr_boot_i90[0], addr_boot_i90[1],
			addr_boot_i90[2], addr_boot_i90[3],
			addr_boot_i90[4], addr_boot_i90[5],
			addr_boot_i90[6], addr_boot_i90[7]);
	mtkl_write(addr_boot_i90, 8);

// out:	8	00000000004c2000 --> length: 004C2000
	printf("Length = %x%x%x%x%x%x%x%x\n", length_boot_i90[0], length_boot_i90[1],
			length_boot_i90[2], length_boot_i90[3],
			length_boot_i90[4], length_boot_i90[5],
			length_boot_i90[6], length_boot_i90[7]);
	mtkl_write(length_boot_i90, 8);

// out:	1	0a --> imgIndex:10
	printf("imgIndex = %x\n", 0x0A);
	mtkl_write("\x0A", 1);

// out:	1	03 --> Index_s 3
	printf("Index_s = %x\n", 0x03);
	mtkl_write("\x03", 1);

// in:	1	5a
	mtkl_test(NULL, "\x5A", 1);

// out:	1	5a
	mtkl_write("\x5A", 1);

// out:	20480	414e44524f494421606642000080001011af090000000011000000000000f010
// out:	20480	6c69746572616c2f6c656e67746820636f646500696e636f7272656374206865
// out:	20480	e99ec0fb6bd1afe4b4a3fbcd0f8a58539bbdd390261843beefd62877eff4127c
// out:	20480	abf778a243ac3743673550471d64bf31e0f978662a801df0fa018cf103c0d7af
// out:	20480	adb37a8e79de9555210df2b647a5431da06a6ecab5e6268b4716f3c51af36ad0
// out:	20480	872698d43d7fef0fd28dd18b2a8db5850d7aba765b2223f0b8aef93724ef2d7d
// out:	20480	e61409db83be7547674566a74b9ef97bc3ebec5eacb17cb43f09d8439d522e17
// out:	20480	2ff708fdf38c23cc5508b3e2088f13b0becaf7a1afb4448ffda2255aa1e69d25
// out:	20480	b97a01fd7ff6ecbaa42bf4f2c0837876c55e1878620aee9d6f0e6c471f5d9117
// out:	20480	584dd7d7ad44dde0a584ed7829be21ce3b49d5256415a85fe8186ac2c7a8d58b
// out:	20480	06e6ba5ad733e0d5bb0750bba792e4ca903e3cc6617b52dc56241644197dde3b
// out:	20480	1d874ef7a7a1a1dd18eb7e193735e60df19ce27a0e71cbaad66e5e41eb82b92c
// out:	20480	11980b7bdae7b20ef56e73873da877d8fb7ef94ce6283edfceb726a26e5e724d
// out:	20480	e7e5628ede3d43e671d3d5fb0a44cc55c864eea8f720bb27cf1dc143dc9da372
// out:	20480	d7099f13e777c3a6d74fcc7cedc83225675caff0649e585c982852d8ae4f311e
// out:	20480	08ae91b9f4169458de4bdb48d797724e2fd4b47de52303f64c9c2b79dcf33de4
// out:	20480	49ec33c6a39118dd586fae35f39c4d8975661aa29d19e2bd1af0d3b5c122e1af
// out:	20480	d38ce72bf4f326fdfc093c676ef4659e217bd119633b32b4b55a8a67a2fb147e
// out:	20480	193fb17ee33147c689f9632d5674baf461a63eae0efde4da7d065d62d8e5c166
// out:	20480	d80fcac64bfc63ce8d2aecc753ee8859954681e0e1434681d04b65fbf14cfa61
// out:	20480	d5cdf9af5579d055fc07062274b6e00dbcc40fcc547c41e9a8e1b92b4269f07e
// out:	20480	8718e39587799e2c9ced6c6b57f1830f5fe5cf2cb2fd178d2e3fddf7db58043b
// out:	20480	b384fafc11d1ae4692c18e5e7c42c67772814c9271b9d4ae820e87bf72702889
// out:	20480	2d6b635aeca5b6314f1b8ad9654afb21f8716bd2a7fa32c63cbacfaea2b3d44f
// out:	20480	978f8a6bf0c3facb2dc319eeabc4c6d825fbe6e403f81bd55d89bc7f72bd3cfb
// out:	12288	1db3f230a6bac0ce28d4dea9e084a7f95d41f37b521530739dcce1ce1cb912b3
// out:	20480	cc57181dafe85d65095fa3c67e37c9eb0fdb91e7a7f2de93c11a90e7be88c1b9
// out:	20480	77e772e6cc73ce79ce737d3fd08dffbd9a6b932527c8ba22b75d2972eaecf13b
// out:	20480	0aec6be160b242d6c9466c0dde617c60a955a52fb5fe7925e2867b19ef19795d
// out:	20480	74bce160dfb43b6bfe5cef983f758ef903db0fc6de39ee980799b10f317fe3b1
// out:	20480	c06052e6f6bfea09776f48d27b3ebdc3f0a43bccde028fa1a4d7da33e7cc3026
// out:	20480	7cffe7c05f89ca5cef89c5e3a01be06d3cb1b52674d1b061407fb5dc23f24041
// out:	20480	d47b2de45defa4dfd84fa8de6bcc8b0197ee5a4b9f4ef4775d8ba9e9d3ad3ce4
// out:	20480	2064a5e4401b3decf32ec9cb60272e29870dec520bb6f5095fa4f3905b5a41fd
// out:	20480	9874b22b11fb4ee398463a34f20376d27c2448e67d99e87eaf9fe645a179a133
// out:	20480	27f3c515ecc7f11f799687f5b381cadddf039b4827fbef329e15ed1bd85c1063
// out:	20480	29c7cf94f388c15797e6724a04b8e09d02bb5bedb48b490f3df74e1a03d2dbdd
// out:	20480	6b8d767a0e22cc9752b620ae9c371053bb82c69df3bec8f164def92bc43b0776
// out:	20480	f0dd960e9a0760846c8cbfc6b8a9e04573689d9f09be5729e1fc4599a1d7ec1c
// out:	20480	656abb587e54f416d0cfc4467d97f9e53f2347c46ac6cd42ee8d327a76b0cd6b
// out:	20480	c650a8d90ec577eda7efc5dfb7f8951f04fab351787ae50bf9acfc56ef799ab8
// out:	20480	bf5ac56e213a133e132e165c17b1959b719b3e751257f62ae59b5a916f02e223
// out:	20480	6b34c27dfc39ac6bea459bcf703eccd1b4d9da61533f19c49ad2e787f38ee01c
// out:	20480	b3b94e7c3b2a47ad381f38637c22c21a4c336ca9ad1a516366dfa9f21ef7e8e9
// out:	20480	79ee1c8c9dcf354c5475b5c5f6857572ff7c2ade11fba9f53f194b76e95cb7b9
// out:	20480	b9977ea29a7827ae754a8c197fe7b3fc3d10bfca2b8d9738c1e99dbdb72e55ed
// out:	20480	9e86f11dbfa0de65cdc7021619e30582829d6994cfd41807dec942cdc8413e71
// out:	20480	60f7597d790033d6e3b39bba0caee146f33348e64efdfb9043c962335fb38451
// out:	20480	ec007ce1e7a106f77411ccd65aea6fee3cd41757b232eaa3dc46cf362ef1246f
// out:	20480	7b27e93d40fa521dc4dca02e96324d6a3ee426c8540b203fc56a07e28f901759
// out:	20480	9800dfb9f191b82a4b79e2f27401b1475282bff25e49b4c32b873c415b88e15c
// out:	12288	cb8422add9fdd82bf9cef4b33df980e44158a570bad0c6e60acc07733d71b72a
// out:	2	e692 --> how have we to calcualte this :-|
// in:	1	69
// out:	1	5a
// out:	20480	eefb26ed054d9cebf78327b9837ca0a1edaa8fe0d918ee7f08e74a992f6aae7d
// out:	20480	7f3610fb547c75ec3d6e219d0e17f8f1fb758e782f466fd931e5b3a978eab982
// out:	20480	7db625ffa0b2dbf18c1cbf8ffbffaaef334e84e3195cd03309f6a7e29db6bc71
// out:	20480	1344bfb79794d84581f7dac4a73fb1d09a06385e9501af3e7536fa5d685d3572
// out:	20480	653f18c8fe8c6718ca1d12feb9a01546f4f7772f0cf5f76f80935750ae41b5bf
// out:	20480	83e97b055f2d7c0093f7aa9c598b4a68f70a1cbf037b61f745d8c3cc7552a074
// out:	20480	02b57ebbea283bc1dc1795387d75dd657836a1afaea56c59ce9c79394744ce63
// out:	20480	5d6a6e7dc667eeea47fe11738c7a4facfea89fabea50a942afdaff58da2f6f3f
// out:	20480	ae2ad9902b562be6c02e8f679e8ad52a5d32f17b8ce485ae2a4d57f3c04bf89c
// out:	20480	eee4b59633791ab1731f1f62dc24be1fa33f3e548fb925e929efe0fde4a5c4f7
// out:	20480	74cfc8c879c53ea78e24fd6e9cdaef1631498a69f4308d7e6a9a2da3f870a95f
// out:	20480	b547d0a99be97f99cc133ef0163e6ce4ae13a989d4ee3333756e4e5e4075467f
// out:	20480	3da66c304f5ccbb26a02d71d52f6635c3e4535aea0bcf01c6d2aa774eccb4065
// out:	20480	d24eaa530bdae824beb144f4f5efa98cc5f40cfc61a1fe23b3265660e21c6eb0
// out:	20480	a9c93870f60aea8bcf88d782ee16fadad8589f0d9ded02bdde6e0dfd5abcf715
// out:	20480	c17906193148fc534d653b638dc37fac91eefdeafe1385d58c3dbc1a582119df
// out:	20480	d8d91c83fa4afb22ea5bccbf5ba95f9b949c80185bfb19fb6b973a37d84572e7
// out:	20480	13cddb33d5f8479fe9a4a9df3f5d6f77c74cd032f4413aa6701ef5d80b562eea
// out:	20480	8c763741db009577907edebd39e1f36cf378fed798f1f4a7492bc6b46ef735ec
// out:	20480	8674485c059f1df7701fbd1865fb7a58c9914b886b876fb97eb7ce6a76e9bb1b
// out:	20480	3f9ea7fb516100c49c9d4314e67fdffc33b441e2434584a712fa27f685a0137f
// out:	20480	87519fa3fdb05e2f31e4feb170ee4cf6af167747d9c6d562f2a2c5e2a751b66b
// out:	20480	67b99aef340a33d42266a3ecc555efa5bdb67fc6709e1803b9a754ae41de317d
// out:	20480	f1f0ee6f788fcd75c935c9b1eaefe07a6c4ec9181768e7d558e7a44553f4bafc
// out:	20480	33abc659c62a7c67ae9dc0d172c65db685f31af02263719e8f63dbb7e7da3538
// out:	12288	509a312cdab52f2063b01529f9944b9925cb4e7ae6cf2dff5036b6581ba987fa
// out:	20480	f918e6971d97fdfa64cc9b29b069c3bb5e8e6dd20d89af47dd9c4f744b1f0b01
// out:	20480	71af137383af0be26be449dd92d5fdfc14b9327e379ed7f2f4ea84e4485d93fe
// out:	20480	979a129ba1d797b6def525d46f7d898b8bbe72223305616f23ff455ac7faee23
// out:	20480	369f9a6de601ff58f5a426114ffc139567c9f9f6774afbd65baa8fafd91c2779
// out:	20480	29cfe59cbb81fd6a7db506f877747badbec127ec50c0037427a87ebe3ac34bba
// out:	20480	ca82cf06a7217e8ab10e70de9376fe9932fd5e5aebd31a86df237fd801f74919
// out:	20480	3adc92fbfad05a3cda76e687cf9bf191e853f0621b3f1f1c7fe98088096abd7f
// out:	20480	9c2cebef60f9f058291ff6487c8aa15845c46386a55d5a0963e511cd3dbdd928
// out:	20480	1a1fa9d7b8c7384eadf13aacf13af2af47f6fe51ba4e73b1ae4e45d9699ae258
// out:	20480	b724be02f731ee6b1d32cfa8db57aad254ec75dc19f3c034543c37dbd7977a32
// out:	20480	a7104fc64b7fa1f77d9e4cfd27f15cdb606fd23e3b2a1ea8927519a37024de7d
// out:	20480	98905adba5e4f6cb456782b980b432c88b23cf6c73dedb469c9361b2cf4683ca
// out:	20480	73037ed3a5371b422f1e6bf934559f4b9febf7e19dcf3ebda199b6e959c0f9f4
// out:	20480	7996ab3cd4b5e954f3a502f8378667dd2f75dd78c6f5c7364dc428e6fb12bc3f
// out:	20480	8dc74659cffc399ea46c913293ddf35e481ece9e9aa4cdf3dee8541193d2031c
// out:	20480	4c4b7f6a46ce4b43c5b9ba166d11e7e29c4bef527711d2dd8374d77793470f50
// out:	20480	a68f5ca0de0c089b2af817f708f8ae157a2167c49a35ed911d4ad75c87d04313
// out:	20480	1df57bb7ba521be80f883822f47f5b6fb822b7c7fca83be5ae5a2452447b3b97
// out:	20480	89da038a31d6058d93a2a58de91808fb4fa0333c00635d60f2c901c927672a3e
// out:	20480	0d95732e4b5b736c12fd9997b6466b116466e1aea19d9946ebc7e16b846dc7f5
// out:	20480	e340fbfd2c0e3af13bccd76311cfbf47a83edd96d31ac67cf7713bddacdf3928
// out:	20480	c3790db4736bba638b452edcecb45fbed3f2ccb3afbcf758a5b17a3cceed9087
// out:	20480	e5383f5dc2709cef91301ce7d795301ce7a74a188ef36b4918e7bb6209435d9e
// out:	20480	ecb9c0f63f27a8f006371a230e7edd7523d9601d3cfe0e1fe9e51ce363b7d061
// out:	20480	6ad7cbfd5fc18cd560ee4a726df15e9b1efb386493101f27303d495e5383498e
// out:	12288	268404c2728475086b10b6221808bb10b6237420980847110e2268f7cf30fc08
// out:	2	0600
// in:	1	69
// out:	1	5a
// out:	20480	dace7d3e932bdaecafc5b774f3a5bf11e32afd6a241877d9373c917d4cf81b99
// out:	20480	b63afa82c7ffd6e703e973150d5587bd82ba0e9b91f6da71f27185828fe3d9f4
// out:	20480	194e1632f626caf183194791f19d858c0d8972fc60c637c85897e8f8f8b13451
// out:	20480	bef061cf3e5b07d27fdbb4fb90f66f96b4ce497dd372dd925cb30c443d7b74f4
// out:	20480	0a6d9478476320f7621e02bf41dff1728d0599907e2f7c9688f3cc9a725d35ca
// out:	20480	1f351b343782660edc3314cdec7e657a0334cfeaa6fdfd3079ab311334ce00cd
// out:	20480	9efdb2caa45d80c6ec16d13f7cfbff0ec68ffc7e9a7f35d76336cb3085eeedd9
// out:	20480	580b3e7fbdb9bb24d243f919c2f2fe533d897cecb511fccfc6dddb35d72fe567
// out:	20480	391bf3d87d0e4f9b46f3b0d05cc0b5c9e169d368960b4d26d6c1394e7ba5d1fc
// out:	20480	7ba82f0f98f6debe8fba5c15ea54f91e6a2cd939c4a8b78cf8df9255f19ae498
// out:	20480	973dbe05618e2db704a82b3a8df7a5f7bbb4cbe99f3e93be533847d146dd7eec
// out:	20480	d5e101c3659fae0e8c0f7bd57aba18ebe98fefae97f555be40d41775b9526c75
// out:	20480	1ccc277e46da776bfe96889f91d06eddcf09e2fbbd4ef311a70dfb88ff1c3d37
// out:	20480	6e6672941d99c46fc183ccb3c6865cee2c217b38cfca815fde45ceb33cf053ce
// out:	20480	16896bfcb2feff846b6a2f9278863886ebac0b73d8a06ca8d34ff9b177a87f1a
// out:	20480	3e50f062d30a90871ec27a4fe8b2c419a3a03c63d45265d7f38c11e644c0ecb0
// out:	20480	91accf6c7152b30d81a3f59adf15bc99b02b12ed7262a0ad4b01bba5a0317b2e
// out:	20480	37d126baefd3589c57e371cc42387815f77fede85ba8d326b483a66752b462b5
// out:	20480	6f647db1daeb304d772b96fbdd06cb8be26e7d2df07e19b0e7b6d95b10e99f57
// out:	20480	3613280f35c71da76443398d251e9472ca051ef4e4c46cde9085f53713fcfb90
// out:	20480	1ff087286be095c043942f703ff0fb5a64f95503bf9765003c007c25f05ce0b5
// out:	20480	b3b459d939d04b37dc1cfbffcea22dce7711e73b19727c9a1a65ff23db2edaf0
// out:	20480	1d3d63a3e8992be849bce59c287adedf063d63a3e849f8cd8fa2a751d0f3e1df
// out:	20480	198adba30ad206fe072e556bc5ffa7e9f97feb3c85df9e79a0a38eef88e7caf4
// out:	20480	c655426bf87477bf0da47f1eff7660adf6e57afbe306d396a3913659a411b036
// out:	12288	f17c89095e709c9c23b31cf3e59aff42bef89663be5c21e7cb95f67c419cde40
// out:	20480	0f77869387031e4190f696d90dc4c3b5b2fd00fb8752df7978df6961bc92ac60
// out:	20480	a003b6f3823ee11a990ff16aa5c0532891757e89fe47cc97bc80015ca8a34fcf
// out:	20480	37130f1785cc41fbde65acb1a8cc45c83339b7329ecc125fd748fcc40dfaa1bd
// out:	20480	af5ff30575bd7486e5d75d96ab3696c395dd5e29d7a147c5f6d8ff4739f08a0d
// out:	20480	06d3abe93ba2c98f3f71b427f537ba0e92f91d60435c41e7ca1199c76e3a8d6d
// out:	20480	d02f07379c4ccd51446e05b6cb57a35609e25241cf4c67e78dc46b96c04e5aea
// out:	20480	1bb1dd3ce8da6e767f1fb86ebbf9f168ba523f3ea6fb8e7be939dd1fbe9a9e55
// out:	20480	2a669df2073d77be8af18bc92e1815dfc4dc11a05d37013e92ee485c27d55765
// out:	20480	bc4de6e8e156fcff5107f877e08d9fa2ed6772da9e90d376f9ff631de089988b
// out:	20480	382cc2b4d61a8c6e5e9bc089078d0947daab7e315dcb797bc3c62fc8bd2de193
// out:	20480	3dfaa170aec720b6d49bdbcb3ada44827ecbe9b7887e5b4f0f6294f74c1acc8b
// out:	20480	c55c57b943df9bdc64ece236a03dd0e9ec365dd7afdfa7611398046c05695f6d
// out:	20480	c941a8e998eb3f1c094c9b2d2ade332871f53007396794f4b5c18c0926752fbf
// out:	20480	532714b6826dcfc27d794a1ec2fb56130fc1fb6d7c48d86a1003f4173566fd6d
// out:	20480	d3baa53d9878414c453e738efbd45afdd612ab0ef9cbadf1e2fcdd44c7e9dbb0
// out:	20480	9da3182b7df80cfea07a60673488ff70ff9b7a32dbf6efea49283c8ce58c4ba7
// out:	20480	a6465b7c4d2ae9f1a4705d03e720d334c4ccf81cdd425e8c75c2c4b3adcbade2
// out:	20480	1b1f7e155746b93e9036b38bfaa79df7d28021f45ed898bef9e06b1c1b0dbf19
// out:	20480	82caf950b77613f4bc6f1e5598443966b505fbda407daff81595c1fd0f3a3b5f
// out:	20480	9800df6ac4e4a44b46350bdff5c9ead5870e4bfd14ada12c63a9bdd9b784f511
// out:	20480	1a21e42d58d785157750d1042f3e2f11f139f0ed053e52bf88df3ad570d8ade2
// out:	20480	2830ce5419d7beef4361d3ce67de6af5e7b28f219ea1e664573a3b86057df24b
// out:	20480	9d497f387e0f63b0a3dfa023de158c32cf7e12f911690e8fbc57da72afdd664d
// out:	20480	43c00e573918811d6e35f708ebae1ecefd9800fed0c4eebef211342113cb003a
// out:	20480	dfddf9665eaaceac49652725ee7d2fed9f7c7a6ee458a3ec1655364b95fdde88
// out:	12288	46737db4b555e09980b95b8c31b70a3cb3c9dced74546d458c45be576fc4e298
// out:	2	7df4
// in:	1	69
// out:	1	5a
// out:	20480	7ab8867d9166a4afe09c7ac3b84cc863d79b124be24d4dd211f206c96767a26f
// out:	20480	ecfb6a2faf7bf2bb3d17d927330b46b633ed926ec0b2ebe800681f6b2cf133e2
// out:	20480	f81db38ef58db3a1ba8779e3bad1230217a4ec53e9e79fd8447847daee81ce8f
// out:	20480	fe52da774594b7d4cbebf507fc9ec17d7a87fda0a438df42c2a7f004bf27fb7e
// out:	20480	ccdfa2ff1b4764f618d631e6fc943ef6d8d30ff5dc631119eb1c85c11d6fe3b6
// out:	20480	e3cfb3dcb3c292270b26e84a87f9023d9f28cfc11ff8e539cec4f18cc93c25ee
// out:	20480	72ca19ef654af24edbb312b9b3bac7f6ccf29599677b863d3493e545f59f7526
// out:	20480	28af63b60c77e0492c18ddaaef7357870133c051b8bfb634523f9410cdb19468
// out:	20480	cadb172caa984d7db618fdc0c216112d713ba194bb7a83917fd9a24a5f39c1d1
// out:	20480	c800b28c0cfef7b9736bd246d646824f493b398b4c248de44c722af9807c48d6
// out:	20480	2c6e93f44aba240d12af64bda45dd223d928d925dd2bf5487ba5376487656765
// out:	20480	46aa4b5aff3b30641889d3e83406ad925645ab903164a7113f2dab9431915646
// out:	20480	bf2f8ee393e53f31a2df3e59febe38914ed83f31b1df3e59febe388e4f96ffc4
// out:	20480	65b6db6bd2fed77b46f579b2b42a0c0826d184be1fd4974f424696c22e082f66
// out:	20480	61a27e5fed69953b5da48d61ae6e4ec61b172ab64af1b56cb576e9426497c153
// out:	20480	7e35b72fa0e6f674414af77835cf7f40cdf31f52f3fc426a9e5fe4d32697372c
// out:	20480	caf7666b5c6d2e55df737f8d1c686c910793247fdff30fcd619ac45667e05a39
// out:	20480	a4182350a2e5edce98711628d390f69e334c962235eff0b3c80073ac137fa735
// out:	20480	53215aa70e5a899324cb122b94a879a67c34d14a9c0eba48d2433f430c334e86
// out:	20480	f67ccdf79949923dc8324489cbf8230f31ce7b4cd9c7d926ca6624d98d7d19e4
// out:	20480	9e7486d6f4bf422f609d639bd66953f6ff69da75c0a7f0ab5b5393677384b691
// out:	20480	8414cefd5852c2882bcce610845d1a7c1d4afc443aab06db5cd37f4d9128afd8
// out:	20480	9f6609e4f485048c38a432c1304d9adf692da4a62e1868a16e45e3c3b320ea2e
// out:	20480	688a20ee89b1ed323cb7e412a7d83ed7cc1afed281773d07c7275bd21dc84f1a
// out:	20480	d35c8e9cb6b5e769343109331634267c125683244e69bd5e0ac6108c3456b2b9
// out:	12288	0ee9f05f31b3a526997b121b74f9bb175899253063d9ebefff9177aede03ea33
// out:	20480	d797a5de26b04390227df48a2ef95a8a2fff67d82d9c42a2554a7cecdd7eb300
// out:	20480	6e27e0665cfd679ad896d91b3f97c72392a9d2426da2f36a726f736beb5f8dcb
// out:	20480	20601c11e3bc5dc377ffa086733d287bc269a7c7c9e3616888972523306d7f68
// out:	20480	9fab1e32295564c9f0cce8f85d9f4ae99e617e108c4bb0668dc4cfa6e213edd7
// out:	20480	6eff53839a83d92c361d0af2216c4c32053cd8b25018ad8bdcc3970501853e07
// out:	20480	202ca08de0f2fa57e1283bc2ec60aaa3780aefdb74cf966f42e12addd3b227b9
// out:	20480	06b2c74b777eedf54fbc420a598640a76bf3c67be6eaeffea0474873c6345f9f
// out:	20480	2c35a24da08d650eac02b3864fa1aa5c71b4c648bcbb96b7f0b2fa9ba92f2764
// out:	20480	5b8496337d6283d2d58da0327d9cbed97a216576f3c12883640c0a762d6c01a2
// out:	20480	87711b6280fdd7b06e9578249770e87cbcb3d3fba65f870627b61556cac2a98e
// out:	20480	e5c3d1a6c9ed3208ab0c4e1d7dc827a99ccf467b006b8370b96b9a9ede8ffa93
// out:	20480	beb33e684554bb7bdb5bdd57f83bd83ad83ef8b8754cff6e1d137aa07f765e6d
// out:	20480	581933c8abacd324a2b9d8ffed58243dec1f5746f5372ae6b748acf839d8c05a
// out:	20480	2761ffd1bfc75002f875028135693f62858dea84297610ed289d471089f12200
// out:	20480	74f732f280ed26e0ccfaf6bacf1f927128af68cb45799e2288509408d41be3c1
// out:	20480	159e920fe41e476cf968d785a6c3784ac4618c69333fc4139dc551bd55c5216c
// out:	20480	4432cc43df9db8a09c24a7a24e1f26321f4e84789631031130f842e2efabb561
// out:	20480	ff7de723fe50f153623a80503a8079d08705dbfe6c98e0503379643d0115858d
// out:	20480	4f587f6ab466c269a744eb3dfc82862f27a83f7e74e5a8689d86be6564b4ce85
// out:	20480	17ebd981a9beeacbba930d7f41ce7e7080a6d27fdbf31bf4e332cad77b8c668d
// out:	20480	98f2846dd0cad5b79c84f4a56b44f9c89309e8ad0f0312f79f0e293ed1cd372a
// out:	20480	8e96e6dabe4e73e7172b4b16d8d5d7df797db953cd2f90895b27c07f114df337
// out:	20480	fbeee298cc9e38798d1ecd3746dec6fccc8646db36b740d9338b9b07f8f4e23c
// out:	20480	36f7e07960573ff8e7264ed3615db941cd530bd2851f4de7b514ebb61469d9ab
// out:	20480	49663d37221dcfccd51ee24f6d16f74a29caf819fe536e3b6359e24e710ccfa6
// out:	12288	35e4f9f5d7518fe64d0e8b01d831ab4d8c09d0517f55291760e82b2794ae79e2
// out:	2	f3bf
// in:	1	69
// out:	1	5a
// out:	20480	36dee685a8e3fe803a3e0c51c7725e477c5f568709e2333dd481f6dc5243d431
// out:	20480	c0d1117d5859e657716591bdb227bc558a7f6248b2be8aba6cd67bafd400075e
// out:	20480	ee080005483c90f4204941b28354364605f8406a43681e890a8880b4877010a4
// out:	20480	055c2ee02a01d76748df0c3dcee1c4fbeef94bcec24c72f3ffec31f42767df0d
// out:	20480	5bc4866424c1436e147ef483f9270e1b36d8c461607e038f87fb6f5be0d7b5c3
// out:	20480	b24db835b06826ac3dea607ef13b99fda49934a568d21e84fc220fefa89a7401
// out:	20480	d1ffa827949c5b433d1ac93a1218c4702b7a70ae3a976bf7f3f9bd9a9fa92bfc
// out:	20480	9ee16e3d3860f76860d5e12c6838e8dee532dfee22bc1de40dcdfe8ac140cc0b
// out:	20480	2b23a7aaaeabe230215d2f9f82410b653b41d1de7f9d59c1f2be9e9c45e46cd3
// out:	20480	ab8de8e1d53c4f0acf13ec9eff3ccf9cdaeff4f07c9e2795e739a8c7cde57169
// out:	20480	f4da3ceaaf4569bcd74adb1cda010da07ec7f9a1bd003a6166e27ed5ceeaffd6
// out:	20480	6cd5131ec556e8d1cac168a55677d818c5f54e3a653982b447d259a35731f8d5
// out:	20480	62dd0db9d347bd3ade317349cec327be9797f3ee874fffc95692ff03dbec9225
// out:	20480	436d49d2acc3410cc32126d171f4176aa8a9b760664767e73759a8d24e95df03
// out:	20480	5e178949c21b5cfb14f69be456b9e34d16a6e3d2a62e7e236cbb89c41cbc86dc
// out:	20480	95fc6bf6678da95c2ada9eda0fd31460daee98c989273ec455651f7aff1fdabd
// out:	20480	0ed9d04e154ac446a13ffe674fe7b69049d35b6c8a6d95ad879b76660d36075b
// out:	20480	c5ba04703160a2f0f7898de7ba54aed115c7cfa7cabd5a5fcda77aaef8cff954
// out:	20480	92fe631ff247aaa5357c91f40f5fdf15d0b6849b2c155b00722dc6538fd75485
// out:	20480	f8a0da799865ca34bdcec7dfee2bfeb20b31ca796f10f3de90bca183bceff0dc
// out:	20480	a5c14692c4e57eea52b3cfd306c80da89cd352f87ea7a3b535d7d1ee0a48cdc9
// out:	20480	e740d1ef82359406d5905dd90c9188dc2705c9ca99e4b826416d3f6753289a31
// out:	20480	f11541bf6f93faddd585e707cef3c8b1d7d5d0935a2b4a6fe774bb5ec0bb50b6
// out:	20480	23364c492e8bf33d5c8399ad2474f819f41c8c84ba1fdcd28050ded0a6ac3c9a
// out:	20480	b52719c94d47e22734f1851c35d55290117fa1601ef5709192589d4b2c6a734e
// out:	12288	2948f190943bc40a0f3167c83c5b998bde70b3d59ac7ab351f0de0ddb02c35c5
// out:	20480	92f00c4dfa0dd0ef569281908cd82a99dc963833c92352eff26c0bee1cd696e7
// out:	20480	33802df80c22e9be0a7f670d858b30ef59f33e3e6891f449bae3bd6bda41e9e3
// out:	20480	f5d5455d3fdad8eae0816b30c65de06fb4e665ea5cc82521a3faca934eee0f93
// out:	20480	ebccee84c23e0aaf266c8de958a03c6759e5b1c0ee1ce63b5a614aef73029489
// out:	20480	e4d0bf601e84fa258e317db5ff663c2bff8df0138d231f31bea31f89763ccef4
// out:	20480	13b5d9b40b9dcc05e5d34f970515f141593c95463c0a30335dc927390af31172
// out:	20480	ebb2dce478480eae9e8d3f5e02711df5d39a1e86f07c5c76d00ef05c32d6a736
// out:	20480	b703bf58d8fae469d9ee60bb986e8f45e7052b1e2426294f924c97494dd30d8f
// out:	20480	b7c658322e527c436f5d7b0af4fc696d93c3f578bf7867f5103c7bfafe364332
// out:	20480	2d76d1fe8abbd1fe22716a8e6bc4d42b6e621bac2d52af86ce6270cd90b190f4
// out:	20480	a28e23b63febed8a0ec78dd6ef3a8ffc91f9f6e87f2bb2192b3be3f7c0ff2bfc
// out:	20480	bd6180723dacbb9e29900bb05e060346cf37a9443b60fd4253b3a7fcca74eb0f
// out:	20480	9b7ed77dd8a2ddd08167cb682172c93f6238f4739218d47e3174cca099def6cc
// out:	4096	389ff8fe1263155803a6e5a9159836c96dc0b479bf08b808d80d3c075807be0e
// out:	2	cff0
// in:	1	69
// out:	2	2e35
// in:	1	5a
// out:	1	a4 --> DACmdDispatcher cmd=104 -> write partition table
// in:	1	5a
// out:	4	00000016 --> 22 partions to write (tabelsize)
// in:	1	5a
// out:	1	5a
// out:	20480	414e44524f494421606642000080001011af090000000011000000000000f010
// out:	20480	6c69746572616c2f6c656e67746820636f646500696e636f7272656374206865
// out:	20480	e99ec0fb6bd1afe4b4a3fbcd0f8a58539bbdd390261843beefd62877eff4127c
// out:	20480	abf778a243ac3743673550471d64bf31e0f978662a801df0fa018cf103c0d7af
// out:	20480	adb37a8e79de9555210df2b647a5431da06a6ecab5e6268b4716f3c51af36ad0
// out:	20480	872698d43d7fef0fd28dd18b2a8db5850d7aba765b2223f0b8aef93724ef2d7d
// out:	20480	e61409db83be7547674566a74b9ef97bc3ebec5eacb17cb43f09d8439d522e17
// out:	20480	2ff708fdf38c23cc5508b3e2088f13b0becaf7a1afb4448ffda2255aa1e69d25
// out:	20480	b97a01fd7ff6ecbaa42bf4f2c0837876c55e1878620aee9d6f0e6c471f5d9117
// out:	20480	584dd7d7ad44dde0a584ed7829be21ce3b49d5256415a85fe8186ac2c7a8d58b
// out:	20480	06e6ba5ad733e0d5bb0750bba792e4ca903e3cc6617b52dc56241644197dde3b
// out:	20480	1d874ef7a7a1a1dd18eb7e193735e60df19ce27a0e71cbaad66e5e41eb82b92c
// out:	20480	11980b7bdae7b20ef56e73873da877d8fb7ef94ce6283edfceb726a26e5e724d
// out:	20480	e7e5628ede3d43e671d3d5fb0a44cc55c864eea8f720bb27cf1dc143dc9da372
// out:	20480	d7099f13e777c3a6d74fcc7cedc83225675caff0649e585c982852d8ae4f311e
// out:	20480	08ae91b9f4169458de4bdb48d797724e2fd4b47de52303f64c9c2b79dcf33de4
// out:	20480	49ec33c6a39118dd586fae35f39c4d8975661aa29d19e2bd1af0d3b5c122e1af
// out:	20480	d38ce72bf4f326fdfc093c676ef4659e217bd119633b32b4b55a8a67a2fb147e
// out:	20480	193fb17ee33147c689f9632d5674baf461a63eae0efde4da7d065d62d8e5c166
// out:	20480	d80fcac64bfc63ce8d2aecc753ee8859954681e0e1434681d04b65fbf14cfa61
// out:	20480	d5cdf9af5579d055fc07062274b6e00dbcc40fcc547c41e9a8e1b92b4269f07e
// out:	20480	8718e39587799e2c9ced6c6b57f1830f5fe5cf2cb2fd178d2e3fddf7db58043b
// out:	20480	b384fafc11d1ae4692c18e5e7c42c67772814c9271b9d4ae820e87bf72702889
// out:	20480	2d6b635aeca5b6314f1b8ad9654afb21f8716bd2a7fa32c63cbacfaea2b3d44f
// out:	20480	978f8a6bf0c3facb2dc319eeabc4c6d825fbe6e403f81bd55d89bc7f72bd3cfb
// out:	12288	1db3f230a6bac0ce28d4dea9e084a7f95d41f37b521530739dcce1ce1cb912b3
// out:	20480	cc57181dafe85d65095fa3c67e37c9eb0fdb91e7a7f2de93c11a90e7be88c1b9
// out:	20480	77e772e6cc73ce79ce737d3fd08dffbd9a6b932527c8ba22b75d2972eaecf13b
// out:	20480	0aec6be160b242d6c9466c0dde617c60a955a52fb5fe7925e2867b19ef19795d
// out:	20480	74bce160dfb43b6bfe5cef983f758ef903db0fc6de39ee980799b10f317fe3b1
// out:	20480	c06052e6f6bfea09776f48d27b3ebdc3f0a43bccde028fa1a4d7da33e7cc3026
// out:	20480	7cffe7c05f89ca5cef89c5e3a01be06d3cb1b52674d1b061407fb5dc23f24041
// out:	20480	d47b2de45defa4dfd84fa8de6bcc8b0197ee5a4b9f4ef4775d8ba9e9d3ad3ce4
// out:	20480	2064a5e4401b3decf32ec9cb60272e29870dec520bb6f5095fa4f3905b5a41fd
// out:	20480	9874b22b11fb4ee398463a34f20376d27c2448e67d99e87eaf9fe645a179a133
// out:	20480	27f3c515ecc7f11f799687f5b381cadddf039b4827fbef329e15ed1bd85c1063
// out:	20480	29c7cf94f388c15797e6724a04b8e09d02bb5bedb48b490f3df74e1a03d2dbdd
// out:	20480	6b8d767a0e22cc9752b620ae9c371053bb82c69df3bec8f164def92bc43b0776
// out:	20480	f0dd960e9a0760846c8cbfc6b8a9e04573689d9f09be5729e1fc4599a1d7ec1c
// out:	20480	656abb587e54f416d0cfc4467d97f9e53f2347c46ac6cd42ee8d327a76b0cd6b
// out:	20480	c650a8d90ec577eda7efc5dfb7f8951f04fab351787ae50bf9acfc56ef799ab8
// out:	20480	bf5ac56e213a133e132e165c17b1959b719b3e751257f62ae59b5a916f02e223
// out:	20480	6b34c27dfc39ac6bea459bcf703eccd1b4d9da61533f19c49ad2e787f38ee01c
// out:	20480	b3b94e7c3b2a47ad381f38637c22c21a4c336ca9ad1a516366dfa9f21ef7e8e9
// out:	20480	79ee1c8c9dcf354c5475b5c5f6857572ff7c2ade11fba9f53f194b76e95cb7b9
// out:	20480	b9977ea29a7827ae754a8c197fe7b3fc3d10bfca2b8d9738c1e99dbdb72e55ed
// out:	20480	9e86f11dbfa0de65cdc7021619e30582829d6994cfd41807dec942cdc8413e71
// out:	20480	60f7597d790033d6e3b39bba0caee146f33348e64efdfb9043c962335fb38451
// out:	20480	ec007ce1e7a106f77411ccd65aea6fee3cd41757b232eaa3dc46cf362ef1246f
// out:	20480	7b27e93d40fa521dc4dca02e96324d6a3ee426c8540b203fc56a07e28f901759
// out:	20480	9800dfb9f191b82a4b79e2f27401b1475282bff25e49b4c32b873c415b88e15c
// out:	12288	cb8422add9fdd82bf9cef4b33df980e44158a570bad0c6e60acc07733d71b72a
// out:	2	e692 --> how have we to calcualte this :-|
// in:	1	69
// out:	1	5a
// out:	20480	eefb26ed054d9cebf78327b9837ca0a1edaa8fe0d918ee7f08e74a992f6aae7d
// out:	20480	7f3610fb547c75ec3d6e219d0e17f8f1fb758e782f466fd931e5b3a978eab982
// out:	20480	7db625ffa0b2dbf18c1cbf8ffbffaaef334e84e3195cd03309f6a7e29db6bc71
// out:	20480	1344bfb79794d84581f7dac4a73fb1d09a06385e9501af3e7536fa5d685d3572
// out:	20480	653f18c8fe8c6718ca1d12feb9a01546f4f7772f0cf5f76f80935750ae41b5bf
// out:	20480	83e97b055f2d7c0093f7aa9c598b4a68f70a1cbf037b61f745d8c3cc7552a074
// out:	20480	02b57ebbea283bc1dc1795387d75dd657836a1afaea56c59ce9c79394744ce63
// out:	20480	5d6a6e7dc667eeea47fe11738c7a4facfea89fabea50a942afdaff58da2f6f3f
// out:	20480	ae2ad9902b562be6c02e8f679e8ad52a5d32f17b8ce485ae2a4d57f3c04bf89c
// out:	20480	eee4b59633791ab1731f1f62dc24be1fa33f3e548fb925e929efe0fde4a5c4f7
// out:	20480	74cfc8c879c53ea78e24fd6e9cdaef1631498a69f4308d7e6a9a2da3f870a95f
// out:	20480	b547d0a99be97f99cc133ef0163e6ce4ae13a989d4ee3333756e4e5e4075467f
// out:	20480	3da66c304f5ccbb26a02d71d52f6635c3e4535aea0bcf01c6d2aa774eccb4065
// out:	20480	d24eaa530bdae824beb144f4f5efa98cc5f40cfc61a1fe23b3265660e21c6eb0
// out:	20480	a9c93870f60aea8bcf88d782ee16fadad8589f0d9ded02bdde6e0dfd5abcf715
// out:	20480	c17906193148fc534d653b638dc37fac91eefdeafe1385d58c3dbc1a582119df
// out:	20480	d8d91c83fa4afb22ea5bccbf5ba95f9b949c80185bfb19fb6b973a37d84572e7
// out:	20480	13cddb33d5f8479fe9a4a9df3f5d6f77c74cd032f4413aa6701ef5d80b562eea
// out:	20480	8c763741db009577907edebd39e1f36cf378fed798f1f4a7492bc6b46ef735ec
// out:	20480	8674485c059f1df7701fbd1865fb7a58c9914b886b876fb97eb7ce6a76e9bb1b
// out:	20480	3f9ea7fb516100c49c9d4314e67fdffc33b441e2434584a712fa27f685a0137f
// out:	20480	87519fa3fdb05e2f31e4feb170ee4cf6af167747d9c6d562f2a2c5e2a751b66b
// out:	20480	67b99aef340a33d42266a3ecc555efa5bdb67fc6709e1803b9a754ae41de317d
// out:	20480	f1f0ee6f788fcd75c935c9b1eaefe07a6c4ec9181768e7d558e7a44553f4bafc
// out:	20480	33abc659c62a7c67ae9dc0d172c65db685f31af02263719e8f63dbb7e7da3538
// out:	12288	509a312cdab52f2063b01529f9944b9925cb4e7ae6cf2dff5036b6581ba987fa
// out:	20480	f918e6971d97fdfa64cc9b29b069c3bb5e8e6dd20d89af47dd9c4f744b1f0b01
// out:	20480	71af137383af0be26be449dd92d5fdfc14b9327e379ed7f2f4ea84e4485d93fe
// out:	20480	979a129ba1d797b6def525d46f7d898b8bbe72223305616f23ff455ac7faee23
// out:	20480	369f9a6de601ff58f5a426114ffc139567c9f9f6774afbd65baa8fafd91c2779
// out:	20480	29cfe59cbb81fd6a7db506f877747badbec127ec50c0037427a87ebe3ac34bba
// out:	20480	ca82cf06a7217e8ab10e70de9376fe9932fd5e5aebd31a86df237fd801f74919
// out:	20480	3adc92fbfad05a3cda76e687cf9bf191e853f0621b3f1f1c7fe98088096abd7f
// out:	20480	9c2cebef60f9f058291ff6487c8aa15845c46386a55d5a0963e511cd3dbdd928
// out:	20480	1a1fa9d7b8c7384eadf13aacf13af2af47f6fe51ba4e73b1ae4e45d9699ae258
// out:	20480	b724be02f731ee6b1d32cfa8db57aad254ec75dc19f3c034543c37dbd7977a32
// out:	20480	a7104fc64b7fa1f77d9e4cfd27f15cdb606fd23e3b2a1ea8927519a37024de7d
// out:	20480	98905adba5e4f6cb456782b980b432c88b23cf6c73dedb469c9361b2cf4683ca
// out:	20480	73037ed3a5371b422f1e6bf934559f4b9febf7e19dcf3ebda199b6e959c0f9f4
// out:	20480	7996ab3cd4b5e954f3a502f8378667dd2f75dd78c6f5c7364dc428e6fb12bc3f
// out:	20480	8dc74659cffc399ea46c913293ddf35e481ece9e9aa4cdf3dee8541193d2031c
// out:	20480	4c4b7f6a46ce4b43c5b9ba166d11e7e29c4bef527711d2dd8374d77793470f50
// out:	20480	a68f5ca0de0c089b2af817f708f8ae157a2167c49a35ed911d4ad75c87d04313
// out:	20480	1df57bb7ba521be80f883822f47f5b6fb822b7c7fca83be5ae5a2452447b3b97
// out:	20480	89da038a31d6058d93a2a58de91808fb4fa0333c00635d60f2c901c927672a3e
// out:	20480	0d95732e4b5b736c12fd9997b6466b116466e1aea19d9946ebc7e16b846dc7f5
// out:	20480	e340fbfd2c0e3af13bccd76311cfbf47a83edd96d31ac67cf7713bddacdf3928
// out:	20480	c3790db4736bba638b452edcecb45fbed3f2ccb3afbcf758a5b17a3cceed9087
// out:	20480	e5383f5dc2709cef91301ce7d795301ce7a74a188ef36b4918e7bb6209435d9e
// out:	20480	ecb9c0f63f27a8f006371a230e7edd7523d9601d3cfe0e1fe9e51ce363b7d061
// out:	20480	6ad7cbfd5fc18cd560ee4a726df15e9b1efb386493101f27303d495e5383498e
// out:	12288	268404c2728475086b10b6221808bb10b6237420980847110e2268f7cf30fc08
// out:	2	0600
// in:	1	69
// out:	1	5a
// out:	20480	dace7d3e932bdaecafc5b774f3a5bf11e32afd6a241877d9373c917d4cf81b99
// out:	20480	b63afa82c7ffd6e703e973150d5587bd82ba0e9b91f6da71f27185828fe3d9f4
// out:	20480	194e1632f626caf183194791f19d858c0d8972fc60c637c85897e8f8f8b13451
// out:	20480	bef061cf3e5b07d27fdbb4fb90f66f96b4ce497dd372dd925cb30c443d7b74f4
// out:	20480	0a6d9478476320f7621e02bf41dff1728d0599907e2f7c9688f3cc9a725d35ca
// out:	20480	1f351b343782660edc3314cdec7e657a0334cfeaa6fdfd3079ab311334ce00cd
// out:	20480	9efdb2caa45d80c6ec16d13f7cfbff0ec68ffc7e9a7f35d76336cb3085eeedd9
// out:	20480	580b3e7fbdb9bb24d243f919c2f2fe533d897cecb511fccfc6dddb35d72fe567
// out:	20480	391bf3d87d0e4f9b46f3b0d05cc0b5c9e169d368960b4d26d6c1394e7ba5d1fc
// out:	20480	7ba82f0f98f6debe8fba5c15ea54f91e6a2cd939c4a8b78cf8df9255f19ae498
// out:	20480	973dbe05618e2db704a82b3a8df7a5f7bbb4cbe99f3e93be533847d146dd7eec
// out:	20480	d5e101c3659fae0e8c0f7bd57aba18ebe98fefae97f555be40d41775b9526c75
// out:	20480	1ccc277e46da776bfe96889f91d06eddcf09e2fbbd4ef311a70dfb88ff1c3d37
// out:	20480	6e6672941d99c46fc183ccb3c6865cee2c217b38cfca815fde45ceb33cf053ce
// out:	20480	16896bfcb2feff846b6a2f9278863886ebac0b73d8a06ca8d34ff9b177a87f1a
// out:	20480	3e50f062d30a90871ec27a4fe8b2c419a3a03c63d45265d7f38c11e644c0ecb0
// out:	20480	91accf6c7152b30d81a3f59adf15bc99b02b12ed7262a0ad4b01bba5a0317b2e
// out:	20480	37d126baefd3589c57e371cc42387815f77fede85ba8d326b483a66752b462b5
// out:	20480	6f647db1daeb304d772b96fbdd06cb8be26e7d2df07e19b0e7b6d95b10e99f57
// out:	20480	3613280f35c71da76443398d251e9472ca051ef4e4c46cde9085f53713fcfb90
// out:	20480	1ff087286be095c043942f703ff0fb5a64f95503bf9765003c007c25f05ce0b5
// out:	20480	b3b459d939d04b37dc1cfbffcea22dce7711e73b19727c9a1a65ff23db2edaf0
// out:	20480	1d3d63a3e8992be849bce59c287adedf063d63a3e849f8cd8fa2a751d0f3e1df
// out:	20480	198adba30ad206fe072e556bc5ffa7e9f97feb3c85df9e79a0a38eef88e7caf4
// out:	20480	c655426bf87477bf0da47f1eff7660adf6e57afbe306d396a3913659a411b036
// out:	12288	f17c89095e709c9c23b31cf3e59aff42bef89663be5c21e7cb95f67c419cde40
// out:	20480	0f77869387031e4190f696d90dc4c3b5b2fd00fb8752df7978df6961bc92ac60
// out:	20480	a003b6f3823ee11a990ff16aa5c0532891757e89fe47cc97bc80015ca8a34fcf
// out:	20480	37130f1785cc41fbde65acb1a8cc45c83339b7329ecc125fd748fcc40dfaa1bd
// out:	20480	af5ff30575bd7486e5d75d96ab3696c395dd5e29d7a147c5f6d8ff4739f08a0d
// out:	20480	06d3abe93ba2c98f3f71b427f537ba0e92f91d60435c41e7ca1199c76e3a8d6d
// out:	20480	d02f07379c4ccd51446e05b6cb57a35609e25241cf4c67e78dc46b96c04e5aea
// out:	20480	1bb1dd3ce8da6e767f1fb86ebbf9f168ba523f3ea6fb8e7be939dd1fbe9a9e55
// out:	20480	2a669df2073d77be8af18bc92e1815dfc4dc11a05d37013e92ee485c27d55765
// out:	20480	bc4de6e8e156fcff5107f877e08d9fa2ed6772da9e90d376f9ff631de089988b
// out:	20480	382cc2b4d61a8c6e5e9bc089078d0947daab7e315dcb797bc3c62fc8bd2de193
// out:	20480	3dfaa170aec720b6d49bdbcb3ada44827ecbe9b7887e5b4f0f6294f74c1acc8b
// out:	20480	c55c57b943df9bdc64ece236a03dd0e9ec365dd7afdfa7611398046c05695f6d
// out:	20480	c941a8e998eb3f1c094c9b2d2ade332871f53007396794f4b5c18c0926752fbf
// out:	20480	532714b6826dcfc27d794a1ec2fb56130fc1fb6d7c48d86a1003f4173566fd6d
// out:	20480	d3baa53d9878414c453e738efbd45afdd612ab0ef9cbadf1e2fcdd44c7e9dbb0
// out:	20480	9da3182b7df80cfea07a60673488ff70ff9b7a32dbf6efea49283c8ce58c4ba7
// out:	20480	a6465b7c4d2ae9f1a4705d03e720d334c4ccf81cdd425e8c75c2c4b3adcbade2
// out:	20480	1b1f7e155746b93e9036b38bfaa79df7d28021f45ed898bef9e06b1c1b0dbf19
// out:	20480	82caf950b77613f4bc6f1e5598443966b505fbda407daff81595c1fd0f3a3b5f
// out:	20480	9800df6ac4e4a44b46350bdff5c9ead5870e4bfd14ada12c63a9bdd9b784f511
// out:	20480	1a21e42d58d785157750d1042f3e2f11f139f0ed053e52bf88df3ad570d8ade2
// out:	20480	2830ce5419d7beef4361d3ce67de6af5e7b28f219ea1e664573a3b86057df24b
// out:	20480	9d497f387e0f63b0a3dfa023de158c32cf7e12f911690e8fbc57da72afdd664d
// out:	20480	43c00e573918811d6e35f708ebae1ecefd9800fed0c4eebef211342113cb003a
// out:	20480	dfddf9665eaaceac49652725ee7d2fed9f7c7a6ee458a3ec1655364b95fdde88
// out:	12288	46737db4b555e09980b95b8c31b70a3cb3c9dced74546d458c45be576fc4e298
// out:	2	7df4
// in:	1	69
// out:	1	5a
// out:	20480	7ab8867d9166a4afe09c7ac3b84cc863d79b124be24d4dd211f206c96767a26f
// out:	20480	ecfb6a2faf7bf2bb3d17d927330b46b633ed926ec0b2ebe800681f6b2cf133e2
// out:	20480	f81db38ef58db3a1ba8779e3bad1230217a4ec53e9e79fd8447847daee81ce8f
// out:	20480	fe52da774594b7d4cbebf507fc9ec17d7a87fda0a438df42c2a7f004bf27fb7e
// out:	20480	ccdfa2ff1b4764f618d631e6fc943ef6d8d30ff5dc631119eb1c85c11d6fe3b6
// out:	20480	e3cfb3dcb3c292270b26e84a87f9023d9f28cfc11ff8e539cec4f18cc93c25ee
// out:	20480	72ca19ef654af24edbb312b9b3bac7f6ccf29599677b863d3493e545f59f7526
// out:	20480	28af63b60c77e0492c18ddaaef7357870133c051b8bfb634523f9410cdb19468
// out:	20480	cadb172caa984d7db618fdc0c216112d713ba194bb7a83917fd9a24a5f39c1d1
// out:	20480	c800b28c0cfef7b9736bd246d646824f493b398b4c248de44c722af9807c48d6
// out:	20480	2c6e93f44aba240d12af64bda45dd223d928d925dd2bf5487ba5376487656765
// out:	20480	46aa4b5aff3b30641889d3e83406ad925645ab903164a7113f2dab9431915646
// out:	20480	bf2f8ee393e53f31a2df3e59febe38914ed83f31b1df3e59febe388e4f96ffc4
// out:	20480	65b6db6bd2fed77b46f579b2b42a0c0826d184be1fd4974f424696c22e082f66
// out:	20480	61a27e5fed69953b5da48d61ae6e4ec61b172ab64af1b56cb576e9426497c153
// out:	20480	7e35b72fa0e6f674414af77835cf7f40cdf31f52f3fc426a9e5fe4d32697372c
// out:	20480	caf7666b5c6d2e55df737f8d1c686c910793247fdff30fcd619ac45667e05a39
// out:	20480	a4182350a2e5edce98711628d390f69e334c962235eff0b3c80073ac137fa735
// out:	20480	53215aa70e5a899324cb122b94a879a67c34d14a9c0eba48d2433f430c334e86
// out:	20480	f67ccdf79949923dc8324489cbf8230f31ce7b4cd9c7d926ca6624d98d7d19e4
// out:	20480	9e7486d6f4bf422f609d639bd66953f6ff69da75c0a7f0ab5b5393677384b691
// out:	20480	8414cefd5852c2882bcce610845d1a7c1d4afc443aab06db5cd37f4d9128afd8
// out:	20480	9f6609e4f485048c38a432c1304d9adf692da4a62e1868a16e45e3c3b320ea2e
// out:	20480	688a20ee89b1ed323cb7e412a7d83ed7cc1afed281773d07c7275bd21dc84f1a
// out:	20480	d35c8e9cb6b5e769343109331634267c125683244e69bd5e0ac6108c3456b2b9
// out:	12288	0ee9f05f31b3a526997b121b74f9bb175899253063d9ebefff9177aede03ea33
// out:	20480	d797a5de26b04390227df48a2ef95a8a2fff67d82d9c42a2554a7cecdd7eb300
// out:	20480	6e27e0665cfd679ad896d91b3f97c72392a9d2426da2f36a726f736beb5f8dcb
// out:	20480	20601c11e3bc5dc377ffa086733d287bc269a7c7c9e3616888972523306d7f68
// out:	20480	9fab1e32295564c9f0cce8f85d9f4ae99e617e108c4bb0668dc4cfa6e213edd7
// out:	20480	6eff53839a83d92c361d0af2216c4c32053cd8b25018ad8bdcc3970501853e07
// out:	20480	202ca08de0f2fa57e1283bc2ec60aaa3780aefdb74cf966f42e12addd3b227b9
// out:	20480	06b2c74b777eedf54fbc420a598640a76bf3c67be6eaeffea0474873c6345f9f
// out:	20480	2c35a24da08d650eac02b3864fa1aa5c71b4c648bcbb96b7f0b2fa9ba92f2764
// out:	20480	5b8496337d6283d2d58da0327d9cbed97a216576f3c12883640c0a762d6c01a2
// out:	20480	87711b6280fdd7b06e9578249770e87cbcb3d3fba65f870627b61556cac2a98e
// out:	20480	e5c3d1a6c9ed3208ab0c4e1d7dc827a99ccf467b006b8370b96b9a9ede8ffa93
// out:	20480	beb33e684554bb7bdb5bdd57f83bd83ad83ef8b8754cff6e1d137aa07f765e6d
// out:	20480	581933c8abacd324a2b9d8ffed58243dec1f5746f5372ae6b748acf839d8c05a
// out:	20480	2761ffd1bfc75002f875028135693f62858dea84297610ed289d471089f12200
// out:	20480	74f732f280ed26e0ccfaf6bacf1f927128af68cb45799e2288509408d41be3c1
// out:	20480	159e920fe41e476cf968d785a6c3784ac4618c69333fc4139dc551bd55c5216c
// out:	20480	4432cc43df9db8a09c24a7a24e1f26321f4e84789631031130f842e2efabb561
// out:	20480	ff7de723fe50f153623a80503a8079d08705dbfe6c98e0503379643d0115858d
// out:	20480	4f587f6ab466c269a744eb3dfc82862f27a83f7e74e5a8689d86be6564b4ce85
// out:	20480	17ebd981a9beeacbba930d7f41ce7e7080a6d27fdbf31bf4e332cad77b8c668d
// out:	20480	98f2846dd0cad5b79c84f4a56b44f9c89309e8ad0f0312f79f0e293ed1cd372a
// out:	20480	8e96e6dabe4e73e7172b4b16d8d5d7df797db953cd2f90895b27c07f114df337
// out:	20480	fbeee298cc9e38798d1ecd3746dec6fccc8646db36b740d9338b9b07f8f4e23c
// out:	20480	36f7e07960573ff8e7264ed3615db941cd530bd2851f4de7b514ebb61469d9ab
// out:	20480	49663d37221dcfccd51ee24f6d16f74a29caf819fe536e3b6359e24e710ccfa6
// out:	12288	35e4f9f5d7518fe64d0e8b01d831ab4d8c09d0517f55291760e82b2794ae79e2
// out:	2	f3bf
// in:	1	69
// out:	1	5a
// out:	20480	36dee685a8e3fe803a3e0c51c7725e477c5f568709e2333dd481f6dc5243d431
// out:	20480	c0d1117d5859e657716591bdb227bc558a7f6248b2be8aba6cd67bafd400075e
// out:	20480	ee080005483c90f4204941b28354364605f8406a43681e890a8880b4877010a4
// out:	20480	055c2ee02a01d76748df0c3dcee1c4fbeef94bcec24c72f3ffec31f42767df0d
// out:	20480	5bc4866424c1436e147ef483f9270e1b36d8c461607e038f87fb6f5be0d7b5c3
// out:	20480	b24db835b06826ac3dea607ef13b99fda49934a568d21e84fc220fefa89a7401
// out:	20480	d1ffa827949c5b433d1ac93a1218c4702b7a70ae3a976bf7f3f9bd9a9fa92bfc
// out:	20480	9ee16e3d3860f76860d5e12c6838e8dee532dfee22bc1de40dcdfe8ac140cc0b
// out:	20480	2b23a7aaaeabe230215d2f9f82410b653b41d1de7f9d59c1f2be9e9c45e46cd3
// out:	20480	ab8de8e1d53c4f0acf13ec9eff3ccf9cdaeff4f07c9e2795e739a8c7cde57169
// out:	20480	f4da3ceaaf4569bcd74adb1cda010da07ec7f9a1bd003a6166e27ed5ceeaffd6
// out:	20480	6cd5131ec556e8d1cac168a55677d818c5f54e3a653982b447d259a35731f8d5
// out:	20480	62dd0db9d347bd3ade317349cec327be9797f3ee874fffc95692ff03dbec9225
// out:	20480	436d49d2acc3410cc32126d171f4176aa8a9b760664767e73759a8d24e95df03
// out:	20480	5e178949c21b5cfb14f69be456b9e34d16a6e3d2a62e7e236cbb89c41cbc86dc
// out:	20480	95fc6bf6678da95c2ada9eda0fd31460daee98c989273ec455651f7aff1fdabd
// out:	20480	0ed9d04e154ac446a13ffe674fe7b69049d35b6c8a6d95ad879b76660d36075b
// out:	20480	c5ba04703160a2f0f7898de7ba54aed115c7cfa7cabd5a5fcda77aaef8cff954
// out:	20480	92fe631ff247aaa5357c91f40f5fdf15d0b6849b2c155b00722dc6538fd75485
// out:	20480	f8a0da799865ca34bdcec7dfee2bfeb20b31ca796f10f3de90bca183bceff0dc
// out:	20480	a5c14692c4e57eea52b3cfd306c80da89cd352f87ea7a3b535d7d1ee0a48cdc9
// out:	20480	e740d1ef82359406d5905dd90c9188dc2705c9ca99e4b826416d3f6753289a31
// out:	20480	f11541bf6f93faddd585e707cef3c8b1d7d5d0935a2b4a6fe774bb5ec0bb50b6
// out:	20480	23364c492e8bf33d5c8399ad2474f819f41c8c84ba1fdcd28050ded0a6ac3c9a
// out:	20480	b52719c94d47e22734f1851c35d55290117fa1601ef5709192589d4b2c6a734e
// out:	12288	2948f190943bc40a0f3167c83c5b998bde70b3d59ac7ab351f0de0ddb02c35c5
// out:	20480	92f00c4dfa0dd0ef569281908cd82a99dc963833c92352eff26c0bee1cd696e7
// out:	20480	33802df80c22e9be0a7f670d858b30ef59f33e3e6891f449bae3bd6bda41e9e3
// out:	20480	f5d5455d3fdad8eae0816b30c65de06fb4e665ea5cc82521a3faca934eee0f93
// out:	20480	ebccee84c23e0aaf266c8de958a03c6759e5b1c0ee1ce63b5a614aef73029489
// out:	20480	e4d0bf601e84fa258e317db5ff663c2bff8df0138d231f31bea31f89763ccef4
// out:	20480	13b5d9b40b9dcc05e5d34f970515f141593c95463c0a30335dc927390af31172
// out:	20480	ebb2dce478480eae9e8d3f5e02711df5d39a1e86f07c5c76d00ef05c32d6a736
// out:	20480	b703bf58d8fae469d9ee60bb986e8f45e7052b1e2426294f924c97494dd30d8f
// out:	20480	b7c658322e527c436f5d7b0af4fc696d93c3f578bf7867f5103c7bfafe364332
// out:	20480	2d76d1fe8abbd1fe22716a8e6bc4d42b6e621bac2d52af86ce6270cd90b190f4
// out:	20480	a28e23b63febed8a0ec78dd6ef3a8ffc91f9f6e87f2bb2192b3be3f7c0ff2bfc
// out:	20480	bd6180723dacbb9e29900bb05e060346cf37a9443b60fd4253b3a7fcca74eb0f
// out:	20480	9b7ed77dd8a2ddd08167cb682172c93f6238f4739218d47e3174cca099def6cc
// out:	4096	389ff8fe1263155803a6e5a9159836c96dc0b479bf08b808d80d3c075807be0e
// out:	2	cff0
// in:	1	69
// out:	2	2e35
// in:	1	5a
// out:	1	a4 --> DACmdDispatcher cmd=104 -> write partition table
// in:	1	5a
// out:	4	00000016 --> 22 partions to write (tabelsize)
// in:	1	5a
// out:	88	5052454c4f41444552000e08c4001d00090000000f0000005200a0e300000000 --> differenet :-|
// in:	1	69
// out:	88	4d42520000608fe0006096e5c07080e2030000000f0000000730a0e300006000 --> differenet :-|
// in:	1	69
// out:	88	45425231004000eb0000a0e32e3300eb040000000f000000000083e500006800 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f504d54001a562300eb0a0000000f0000004d5400eb00007000 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f50524f5f494e464f000f0000000f000000524f5f490000b000 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f4e5652414d0000a0e10c0000000f0000005652414d0000e000 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f50524f544543545f460064204b696e67646f6d2e3132353200
// in:	1	69
// out:	88	5f5f4e4f444c5f50524f544543545f530064204b696e67646f6d2e3132353200
// in:	1	69
// out:	88	5f5f4e4f444c5f534543434647002de90d0000000f0000004543434600007002 --> differenet :-|
// in:	1	69
// out:	88	55424f4f54002de90211a0130010a003050000000f0000000040a0e100007202 --> differenet :-|
// in:	1	69
// out:	88	424f4f54494d47000500a0e10830a0e3070000000f00000008308de500007802 --> differenet :-|
// in:	1	69
// out:	88	5245434f564552590040a0e10150a0e1080000000f000000000057e30000d802 --> differenet :-|
// in:	1	69
// out:	88	5345435f524f00e00b6086e001a04ae2060000000f0000000700a0e100003803 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f4d49534300a234009e0b0000000f000000d8e1290000009803 --> differenet :-|
// in:	1	69
// out:	88	4c4f474f0002c4e50700a0e1dffaffeb040000000f0000005702c4e50000a003 --> differenet :-|
// in:	1	69
// out:	88	45425232000000000000000000000000040000000f000000000000000000d003 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f4558504442000000000c0000000f000000000000000000d803 --> differenet :-|
// in:	1	69
// out:	88	414e44524f4944000000000000000000070000000f0000000000000000007804 --> differenet :-|
// in:	1	69
// out:	88	43414348450000000000000000000000050000000f000000000000000000d830 --> differenet :-|
// in:	1	69
// out:	88	55535244415441000000000000000000070000000f0000000000000000001851 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f4641540000000000000a0000000f00000000000000000018d1 --> differenet :-|
// in:	1	69
// out:	88	5f5f4e4f444c5f424d54504f4f4c00000e0000000f00000000000000a800ffff
// in:	1	69
// out:	4	00000001
// in:	1	5a
// in:	1	5a
// out:	1	d9
// in:	1	5a
// out:	4	00000000
// in:	1	5a
// in:	0	
 

	printf("huuuuuray\n");

	mtk_close();
	return 0;
}
