#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>

#include "mtkda.h"

struct da_version_ops da_ops [] = {
	[V3_1304_0_119] = { 
			.offset_da = 0x7BEB4,
			.offset_magic1 = 0x16020,
			.offset_magic2 = 0x9F884
	}
};	

int fd_da = -1;

int mtkda_open(char *file, enum da_version da_ver)
{
	int seek;

//	fd_da = open("/home/linux/vm/shared/SP_Flash_Tool_v3.1304.0.119/MTK_AllInOne_DA.bin", O_RDONLY);
	fd_da = open(file, O_RDONLY);
	if (fd_da < 0) {
		fprintf(stderr, "error: could not open Download Agent at %s\n", file);
		return -1;
	}

	seek = lseek(fd_da, da_ops[da_ver].offset_da, SEEK_SET);

	if (seek != da_ops[da_ver].offset_da) {
		fprintf(stderr, "error: seek %d in %s failed - we seeked only \
				%d bytes\n", da_ops[da_ver].offset_da, file, seek);
		close(fd_da);
		fd_da = -1;
		return -1;
	}
	
	return 0;
}

int mtkda_read(unsigned char *obuf, int size)
{
	int bread = 0;

	if (size > SSIZE_MAX) {
		fprintf(stderr, "error: size %d to big to read\n", size);
		return -1;
	}

	do {
		bread += read(fd_da, obuf+bread, size-bread);
	} while (bread < size);

	return 0;
}

int mtkda_seek(enum da_version da_ver, int type)
{
	int ret, seek;

	if (type == MAGIC_1)
		seek = da_ops[da_ver].offset_magic1;
	else if (type == MAGIC_2)
		seek = da_ops[da_ver].offset_magic2;
	else {
		printf("%s - error: unkonwn type %d\n", __func__, type);
		return -1;
	}

	ret = lseek(fd_da, seek, SEEK_SET);

	if (ret != seek) {
		fprintf(stderr, "error: seek %d failed - we seeked only \
				%d bytes\n", seek, ret);
		return -1;
	}

	return ret;
}
int mtkda_close()
{
	return close(fd_da);
}
