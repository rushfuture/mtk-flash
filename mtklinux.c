#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

//#include "mtklinux.h"
#include "interface.h"

#define BAUDRATE B921600
#define _POSIX_SOURCE 1

static struct termios oldtio;
static int fd = -1;

static int mtkl_open()
{
	struct termios newtio;
	int res, ret;
	unsigned char buf[255];

	while (access( "/dev/ttyACM0", F_OK ) == -1) {
		printf(".");
		usleep(500 * 1000);
	}

	fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY );
	if (fd < 0) {
		perror("/dev/ttyACM0");
		return -1;
	}

	ret = tcgetattr(fd,&oldtio);
	if (ret < 0) {
		perror("tcsetattr failed\n");
		fd = -1;
		return ret;
	}

	bzero(&newtio, sizeof(newtio));

	/* BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
	 * CS8     : 8n1 (8bit,no parity,1 stopbit)
	 * CLOCAL  : local connection, no modem contol
	 * CREAD   : enable receiving characters
	 */
	newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;

	// IGNPAR  : ignore bytes with parity errors
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;


	newtio.c_lflag = 0;

	newtio.c_cc[VTIME] = 0; // inter-character timer unused
	newtio.c_cc[VMIN] = 1; // blocking read until 1 character arrives

	ret = tcflush(fd, TCIOFLUSH);
	if (ret < 0) {
		perror("tcflush failed\n");
		fd = -1;
		return ret;
	}

	ret = tcsetattr(fd, TCSANOW, &newtio);
	if (ret < 0) {
		perror("tcsetattr failed\n");
		fd = -1;
		return ret;
	}

	return 0;
}

static int mtkl_close()
{
	close(fd);
	fd = -1;
	return tcsetattr(fd, TCSANOW, &oldtio);
}

int mtkl_read(unsigned char *obuf, int len)
{
	int count = 0, ret;

	while (count < len) {
		ret = read(fd, obuf + count, len - count);
		if (ret < 0) {
			perror("mtkl_read failed");
			return ret;
		}

		count += ret;
	}

	return count;
}

int mtkl_write(unsigned char *ibuf, int len)
{
	int count = 0, ret;

	while (count < len) {
		ret = write(fd, ibuf + count, len - count);
		if (ret < 0) {
			perror("mtkl_write failed");
			return ret;
		}

		count += ret;
	}

	return count;
}

int mtkl_flush(void)
{
	int ret;

	ret = fdatasync(fd);
	if (ret < 0)
		return ret;

	return tcdrain(fd);
}

int mtkl_clear(void)
{
	return tcflush(fd, TCIOFLUSH);
}

struct interface_ops mtklinux_ops = {
	.open = mtkl_open,
	.close = mtkl_close,
	.read = mtkl_read,
	.write = mtkl_write,
	.flush = mtkl_flush,
	.clear = mtkl_clear,
};


///////////////// from here DEPCRECATED ////////////////////

int mtkl_echo(unsigned char *ibuf, unsigned char *obuf, int len)
{
	int ret;

	ret = mtkl_write(ibuf, len);
	if (ret < 0)
		return ret;

	return mtkl_read(obuf, len);
}

int mtkl_test(unsigned char *ibuf, unsigned char *check, int len)
{
	unsigned char buf[255];
	int i = 0, ret;

	if (len > 254) {
		fprintf(stderr, "error: len to big\n");
		return -1;
	}

	if (ibuf != NULL) {
#ifdef DBG
		printf("%s - write values[%d]: ", __func__, len);
		for(i=0;i<len;i++)
			printf("%.2x ", ibuf[i]);
		printf("\n");
#endif
		ret = mtkl_write(ibuf, len);
		if (ret < 0)
			return ret;
	}

	ret = mtkl_read(buf, len);
	if (ret < 0)
		return ret;

	while (bcmp(buf, check, len) != 0) {
		printf("%s - error:\t", __func__);
		for (i = 0; i < len; i++)
			printf("%.2x %.2x\t", buf[i], check[i]);
		printf("\n");

		ret = mtkl_read(buf, len);
		if (ret < 0)
			return ret;
	};

	return 0;
}


