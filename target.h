#ifndef _TARGET_H_
#define _TARGET_H_


short int target_read32(unsigned int addr, unsigned int len, unsigned char *check);
short int target_write32(unsigned int addr, unsigned int len, unsigned char *data);
short target_get_hw_code(unsigned char *ibuf);
short target_get_config(unsigned char *config);

unsigned char target_check_bootloader_ver(void);
short target_send_download_agent(unsigned int addr, unsigned int da_len, unsigned int sig_len, unsigned char *checksum);
void target_jump_to_da(void);

#ifdef MT6577
short target_uart1_log_enable(void)

{
	return 0x0;
}

short target_power_init()
{
	return 0x0;
}

short target_power_read16(unsigned int addr, unsigned char *check)
{
	return 0x0;
}

short target_power_deinit(void)
{
	return 0x0;
}
#else
short target_uart1_log_enable(void);
short target_power_init(void);
short target_power_init(void);
short target_power_deinit(void);
#endif

#endif
