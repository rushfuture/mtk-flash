
struct preloader_hw_sw_ver {
	unsigned int hw_subcode;
	unsigned int hw_version;
	unsigned int sw_version;
};

int preloader_wait_for_ready(void);
int preloader_handshake(void);

int preloader_get_hw_sw_version(struct preloader_hw_sw_ver *ver);
