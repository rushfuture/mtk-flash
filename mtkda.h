#ifndef MTKDA_H_
#define MTKDA_H_

#define MAGIC_1	1
#define MAGIC_2	2

enum da_version {
	V3_1304_0_119 = 0
};


struct da_version_ops {
	int offset_da;
	int offset_magic1;
	int offset_magic2;
};

int mtkda_open(char *file, enum da_version da_ver);
int mtkda_read(unsigned char *obuf, int size);
int mtkda_close();

#endif
