#include <stdio.h>

#include "interface.h"
#include "preloader.h"

#define CMD_GET_HW_SW_VER          0xfc
#define CMD_GET_HW_CODE            0xfd
#define CMD_GET_BL_VER             0xfe

#define CMD_LEGACY_WRITE           0xa1
#define CMD_LEGACY_READ            0xa2

#define CMD_I2C_INIT               0xB0
#define CMD_I2C_DEINIT             0xB1
#define CMD_I2C_WRITE8             0xB2
#define CMD_I2C_READ8              0xB3
#define CMD_I2C_SET_SPEED          0xB4

#define CMD_PWR_INIT               0xC4
#define CMD_PWR_DEINIT             0xC5
#define CMD_PWR_READ16             0xC6
#define CMD_PWR_WRITE16            0xC7          

#define CMD_READ16                 0xD0
#define CMD_READ32                 0xD1
#define CMD_WRITE16                0xD2
#define CMD_WRITE16_NO_ECHO        0xD3
#define CMD_WRITE32                0xD4
#define CMD_JUMP_DA                0xD5
#define CMD_JUMP_BL                0xD6
#define CMD_SEND_DA                0xD7
#define CMD_GET_TARGET_CONFIG      0xD8
#define CMD_UART1_LOG_EN           0xDB

/**
 * 1st part of Preloader handshake
 * The preloader is emitting the string R E A D Y on the usb
 * After this it checks if it can read 0xa0 from the host
 * and enters the usb-download handler if appropriate.
 * After such a cycle it waits 20ms and emits R E A D Y again.
 * @ref usb_listen in usb_handshake.c
 */
int preloader_wait_for_ready(void)
{
	unsigned char ibuf[255];
	int ret = 0, i;

	for (i = 0; i < 255; i++) {
		/* rolling index makes sure excess bytes in front get ignored */
		ret = mtk_read(&ibuf[i], 1);
		if (ret < 0)
			return -1;

		if (i >= 5 && strncmp(&ibuf[i-5], "READY", 5) != 0) {
			ret = 1;
			break;
		}
	}

	if (!ret)
		return -1;

	return 0;
}

/**
 * Handshake with the preloader and entering of the usb download mode
/////////////// ref: usbdl_check_start_command in download.c ////////
	 */
int preloader_handshake(void)
{
	/**
	 * USB download start command:
	 * Return values are ~input if correct, otherwise input+1
	 * and sequence restarts at the beginning.
	 */
	unsigned char cmd_dlstart[] = { 0xa0, 0x0a, 0x50, 0x05 };
	int loop_ret, ret, i;

	/* entering usb-download intersects with the start command */
	do {
			ret = mtk_write_char(cmd_dlstart[0]);
			if (ret < 0)
				return -1;

			usleep(10000);

			ret = mtk_read_char();
			if (ret < 0)
				return -1;
	} while (ret != (unsigned char)~cmd_dlstart[0]);

	/* At this point we've send the 0xa0 to enter the usb download mode,
	 * but due to the repeated sends of the character we'll also have
	 * already sent parts of the start command resulting in some answers
	 * being already in the buffer.
	 * Therefore reset the buffers to get a sane start now.
	 */
	usleep(10000);
	mtk_clear();

	/* we're at some unknown point inside the start command, by sending
	 * an unexpected character, we cause the receiver to restart at the
	 * first character.
	 */
	ret = mtk_write_char(0xaa);
	if (ret < 0)
		return ret;

	ret = mtk_read_char();
	if (ret < 0)
		return ret;

	if (ret != 0xaa + 1) {
		fprintf(stderr, "unexpected character returned, clearing of queue failed\n");
		return -1;
	}

	while (1) {
		loop_ret = 0;
		for (i = 0; i < 4; i++) {
			ret = mtk_write_char(cmd_dlstart[i]);
			if (ret < 0)
				return -1;

			ret = mtk_read_char();
			if (ret < 0)
				return -1;
			if (ret == (unsigned char)~cmd_dlstart[i]) {
				/* the expected result is ~cmd_dlstart[i] */
			} else if (ret == cmd_dlstart[i] + 1) {
				/* the preloader will want to restart with the
				 * first character.
				 */
				loop_ret = -1;
				break;
			} else {
				fprintf(stderr, "unexpected result 0x%x for 0x%x\n",
					ret, cmd_dlstart[i]);
				return -1;
			}
		}

		/* if we made it sucessfull through the for loop, continue */
		if (loop_ret == 0)
			break;
	}

	return 0;
}

/*
 * Implementation of preloader commands
 */

/**
 * Send command byte and handle possible echo.
 * For all commands except GET_BL_VER, the send commands gets echoed
 * by the preloader.
 */
static int preloader_send_command(unsigned char cmd)
{
	int ret;

	ret = mtk_write_char(cmd);
	if (ret < 0)
		return ret;

	if (cmd != CMD_GET_BL_VER) {
		ret = mtk_read_char();
		if (ret < 0)
			return ret;

		if (ret != cmd) {
			fprintf(stderr, "command echo 0x%x does not match command 0x%x\n", ret, cmd);
			return -1;
		}
	}

	return 0;
}

/**
 * Read the hardware-code from the preloader.
 * Returns hw code on sucess, -EERROR on error
 */
int preloader_get_hw_code(void)
{
	int ret, code;

	ret = preloader_send_command(CMD_GET_HW_CODE);
	if (ret < 0)
		return ret;

	code = mtk_read_word();
	if (code < 0)
		return ret;

	ret = mtk_read_word();
	if (ret < 0)
		return ret;

	/* status */
	if (ret > 0) {
		fprintf(stderr, "%s: returned %d\n", __func__, ret);
		return -1;
	}

	return code;
}

/**
 * Read hw and sw versions from preloader.
 * @ver: struct that gets filled with the read data.
 * Returns 0 on success, -EERROR on error
 */
int preloader_get_hw_sw_version(struct preloader_hw_sw_ver *ver)
{
	int ret;

	if (!ver)
		return -1;

	ret = preloader_send_command(CMD_GET_HW_SW_VER);
	if (ret < 0)
		return ret;

	ret = mtk_read_word();
	if (ret < 0)
		return ret;
	ver->hw_subcode = ret;

	ret = mtk_read_word();
	if (ret < 0)
		return ret;
	ver->hw_version = ret;

	ret = mtk_read_word();
	if (ret < 0)
		return ret;
	ver->sw_version = ret;

	/* status */
	ret = mtk_read_word();
	if (ret < 0)
		return ret;
	if (ret > 0) {
		fprintf(stderr, "%s: returned %d\n", __func__, ret);
		return -1;
	}

	return 0;
}
